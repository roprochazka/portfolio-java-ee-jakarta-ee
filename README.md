Fragments is a dynamic web project written using the servlet and jsp technologies (the jsp pages use JSTL and expression language for dynamic content). 
The project was created for the WildFly server. The data of the web are stored in a MySQL database,
connection to the database is provided via the DataSource injected by the server. The tables used can be created using the initialize.sql script. 
The web project is a music library and articles (biographies, reviews) presentation website, 
equiped with its own content manager (the project assumes only one user with administration roles). More information can be found on the help.jsp page.