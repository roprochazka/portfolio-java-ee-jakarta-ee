CREATE SCHEMA music;


CREATE TABLE music.album (
    id_album integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    review text,
    name character varying(40) NOT NULL,
    year integer,
    CONSTRAINT album_year_check CHECK (((year > 1900) AND (year < 9999)))
);

CREATE TABLE music.album_track (
    id_track integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    song_id integer NOT NULL,
    album_id integer NOT NULL,
    ordinal integer NOT NULL
);


CREATE TABLE music.artist (
    id_artist integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name character varying(50) NOT NULL,
    biography text
);


CREATE TABLE music.artist_member (
    id_member integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    album_id integer,
    musician_id integer,
    instrument character varying(50),
    artist_id integer
);

CREATE TABLE music.history (
    id_record integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    instant timestamp NOT NULL,
    change_type character varying(50) NOT NULL,
    changed_id integer,
    data_type character varying(50)
);

CREATE TABLE music.musician (
    id_musician integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name character varying(40),
    surname character varying(40) NOT NULL,
    nationality character varying(40)
);


CREATE TABLE music.nationality (
    id_nationality integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    country character varying(40) NOT NULL
);


CREATE TABLE music.song (
    id_song integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    lyrics text,
    name character varying(50) NOT NULL,
    original_artist_id integer,
    interpreting_artist_id integer,
    type character varying(40),
    duration integer
);

CREATE TABLE music.user (
	id_user integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user character varying(50) NOT NULL,
	password character varying(50) NOT NULL
);

ALTER TABLE music.artist_member
    ADD CONSTRAINT artist_member_album_id_fkey FOREIGN KEY (album_id) REFERENCES music.album(id_album);

ALTER TABLE music.artist_member
    ADD CONSTRAINT artist_member_artist_id_fkey FOREIGN KEY (artist_id) REFERENCES music.artist(id_artist);

ALTER TABLE music.artist_member
    ADD CONSTRAINT artist_member_musician_id_fkey FOREIGN KEY (musician_id) REFERENCES music.musician(id_musician);

ALTER TABLE music.album_track
    ADD CONSTRAINT cd_song_link_cd_id_fkey FOREIGN KEY (album_id) REFERENCES music.album(id_album);

ALTER TABLE music.album_track
    ADD CONSTRAINT cd_song_link_song_id_fkey FOREIGN KEY (song_id) REFERENCES music.song(id_song);

ALTER TABLE music.song
    ADD CONSTRAINT song_interpreting_artist_id_fkey FOREIGN KEY (interpreting_artist_id) REFERENCES music.artist(id_artist);

ALTER TABLE music.song
    ADD CONSTRAINT song_original_artist_id_fkey FOREIGN KEY (original_artist_id) REFERENCES music.artist(id_artist);
