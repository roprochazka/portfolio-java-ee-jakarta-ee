package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.AlbumDAO;
import database.ArtistDAO;
import database.ArtistMemberDAO;
import database.MusicianDAO;
import model.ArtistMember;

@WebServlet("/addMusicianToAlbum")
public class AddMusicianToAlbumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Optional<ArtistMember> optArtistMember = RequestHandler.readArtistMember(request);
		if (optArtistMember.isPresent() && UserUtils.userLoggedIn(request)) {
			ArtistMember artistMember = optArtistMember.get();
			addArtistMember(request, artistMember);
		}
		RequestHandler.forward("WEB-INF/addMusicianToAlbumPage.jsp?menu=Album&albumId="
				+ request.getParameter("albumId") + "&title=" + request.getParameter("title"), request, response);
	}

	private void addArtistMember(HttpServletRequest request, ArtistMember artistMember) {
		String message = null;
		String title = request.getParameter("title");
		try {
			ArtistDAO artistDAO = new ArtistDAO(ds);
			if (!artistDAO.exists(artistMember.getArtist())) {
				throw new IllegalArgumentException("The artist doesn't exist.");
			}
			MusicianDAO musicianDAO = new MusicianDAO(ds);
			if (!musicianDAO.exists(artistMember.getMusician())) {
				throw new IllegalArgumentException("The musician doesn't exist.");
			}
			int albumId = artistMember.getAlbumId();
			AlbumDAO albumDAO = new AlbumDAO(ds);
			if (!albumDAO.getAlbumById(albumId).isPresent()) {
				throw new IllegalArgumentException("The album must exist.");
			}
			ArtistMemberDAO artistMemberDAO = new ArtistMemberDAO(ds);
			artistMemberDAO.addArtistMember(artistMember.getArtist(), artistMember.getMusician(), albumId,
					artistMember.getInstrument());
			message = "The musician " + artistMember.getMusician().getName() + " "
					+ artistMember.getMusician().getSurname() + " has been added successfully to the album "
					+ title;
		} catch (SQLException e) {
			message = "The musician " + artistMember.getMusician().getName() + " "
					+ artistMember.getMusician().getSurname() + " wasn't saved due to a database error.";
		} catch (IllegalArgumentException e) {
			message = "The musician " + artistMember.getMusician().getName() + " "
					+ artistMember.getMusician().getSurname() + "wasn't added. " + e.getMessage();
		}
		request.setAttribute("message", message);
	}
}
