package controller.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.utils.RequestHandler;
import utils.ImageHandler;

@WebServlet("/image")
public class ImageServlet extends HttpServlet {
	private static final String EMPTY_FILE_NAME = "empty"; // without extension
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idAlbum = RequestHandler.readId(request);
		File coverImage = getCoverImage(idAlbum);
		writeCoverImageToResponse(response, coverImage);
	}

	private void writeCoverImageToResponse(HttpServletResponse response, File image) throws IOException, FileNotFoundException {
		try (FileInputStream fis = new FileInputStream(image);
				BufferedInputStream bis = new BufferedInputStream(fis);
				BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream());) {
			int i = 0;
			while ((i = bis.read()) > -1) {
				output.write(i);
			}
		}
	}

	private File getCoverImage(int idAlbum) {
		String fileName = ImageHandler.IMAGE_FOLDER + idAlbum + ImageHandler.IMAGE_EXTENSION;
		File image = new File(fileName);
		if (!image.exists()) {
			fileName = ImageHandler.IMAGE_FOLDER + EMPTY_FILE_NAME + ImageHandler.IMAGE_EXTENSION;
			image = new File(fileName);
		}
		return image;
	}
}
