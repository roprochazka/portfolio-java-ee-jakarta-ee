package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import database.ArtistDAO;
import model.Artist;
import model.Nationality;

@WebServlet("/allArtists")
public class AllArtistsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			prepareArtistsList(request);
		} catch (SQLException e) {
			String message = "A database error occurred while loading the artists.";
			request.setAttribute("message", message);
		}
		RequestHandler.forward("allArtistsList.jsp?menu=Artist", request, response);
	}

	private void prepareArtistsList(HttpServletRequest request) throws SQLException {
		Map<Integer, Artist> artists = readArtists(request);
		Map<Character, List<Integer>> alphabeticalArtistMap = toAlphabeticalArtistMap(artists);
		List<Character> firstChars = toFirstCharsList(alphabeticalArtistMap);

		request.setAttribute("firstChars", firstChars);
		request.setAttribute("alphabeticalArtistMap", alphabeticalArtistMap);
		request.setAttribute("artists", artists);
	}

	private List<Character> toFirstCharsList(Map<Character, List<Integer>> alphabeticalArtistMap) {
		List<Character> firstChars = new ArrayList<>();
		firstChars.addAll(alphabeticalArtistMap.keySet());
		firstChars.sort((x, y) -> x.compareTo(y));
		return firstChars;
	}

	private Map<Character, List<Integer>> toAlphabeticalArtistMap(Map<Integer, Artist> artists) {
		Map<Character, List<Integer>> alphabeticalArtistMap = new HashMap<>();
		for (int idArtist : artists.keySet()) {
			Artist artist = artists.get(idArtist);
			String artistName = artist.getName();
			char firstChar = artistName.charAt(0);
			if (!alphabeticalArtistMap.containsKey(firstChar)) {
				List<Integer> firstCharArtistsIds = new ArrayList<>();
				alphabeticalArtistMap.put(firstChar, firstCharArtistsIds);
			}
			alphabeticalArtistMap.get(firstChar).add(idArtist);
		}

		for (char firstChar : alphabeticalArtistMap.keySet()) {
			alphabeticalArtistMap.get(firstChar)
					.sort((x, y) -> artists.get(x).getName().compareTo(artists.get(y).getName()));
		}
		return alphabeticalArtistMap;
	}

	private Map<Integer, Artist> readArtists(HttpServletRequest request) throws SQLException {
		Map<Integer, Artist> readArtists;
		String nationalityParameter = request.getParameter("nationality");
		if (nationalityParameter == null || nationalityParameter.isEmpty()) {
			nationalityParameter = "0";
		}
		ArtistDAO artistDAO = new ArtistDAO(ds);
		if ("0".equals(nationalityParameter)) {
			readArtists = artistDAO.getAllArtists();
		} else {
			readArtists = artistDAO.getArtistsByNationality(Nationality.valueOf(nationalityParameter));
		}
		return readArtists;
	}
}
