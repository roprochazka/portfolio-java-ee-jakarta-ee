package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import database.AlbumDAO;
import model.Album;

@WebServlet("/albumsPerYears")
public class AlbumsPerYearsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			prepareAlbumList(request);
		} catch (SQLException e) {
			String message = "A database error occurred while loading the artists.";
			request.setAttribute("message", message);
		}
		RequestHandler.forward("albumsPerYearsList.jsp?menu=Album", request, response);
	}

	private void prepareAlbumList(HttpServletRequest request) throws SQLException {
		AlbumDAO albumDAO = new AlbumDAO(ds);
		Map<Integer, Album> albums = albumDAO.getAllAlbums();
		Map<Integer, List<Integer>> yearsMap = toYearsMap(albums);
		List<Integer> yearsList = toYearsList(yearsMap);

		request.setAttribute("yearsList", yearsList);
		request.setAttribute("yearsMap", yearsMap);
		request.setAttribute("albums", albums);
	}

	private List<Integer> toYearsList(Map<Integer, List<Integer>> yearsMap) {
		return yearsMap.keySet().stream().sorted((x,y)->(x-y)).collect(Collectors.toList());
	}

	private Map<Integer, List<Integer>> toYearsMap(Map<Integer, Album> albums) {
		Map<Integer, List<Integer>> yearsMap = new HashMap<>();
		for (int albumId : albums.keySet()) {
			int year = albums.get(albumId).getYear();
			if (!yearsMap.containsKey(year)) {
				List<Integer> yearAlbums = new ArrayList<>();
				yearsMap.put(year, yearAlbums);
			}
			yearsMap.get(year).add(albumId);
		}
		for (int year: yearsMap.keySet()) {
			yearsMap.get(year).sort((x,y)->(albums.get(x).getName().compareTo(albums.get(y).getName())));
		}
		
		return yearsMap;
	}
}
