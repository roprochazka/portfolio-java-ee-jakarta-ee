package controller.servlets;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.DAO;
import database.DataType;

@WebServlet("/deleteAlbum")
public class DeleteAlbumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int idAlbum = RequestHandler.readId(request);
		if (idAlbum != 0 && UserUtils.userLoggedIn(request)) { // id must not be zero
			DAO dao = new DAO(ds);
			String message = dao.deleteItem(idAlbum, DataType.ALBUM);
			request.setAttribute("message", message);
		}
		RequestHandler.forward("findAlbumPage.jsp?menu=Album", request, response);
	}
}