package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.AlbumTrackDAO;
import database.ArtistDAO;
import database.SongDAO;
import exceptions.ItemAlreadyExistsException;
import model.AlbumTrack;
import model.Song;

@WebServlet("/addAlbumTrack")
public class AddAlbumTrackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Optional<AlbumTrack> optAlbumTrack = RequestHandler.readAlbumTrack(request);
		if (optAlbumTrack.isPresent() && UserUtils.userLoggedIn(request)) {
			AlbumTrack albumTrack = optAlbumTrack.get();
			addAlbumTrack(request, albumTrack);
		}
		RequestHandler.forward("WEB-INF/addAlbumTrackPage.jsp?menu=Album&albumId=" + request.getParameter("albumId")
				+ "&title=" + request.getParameter("title"), request, response);
	}

	private void addAlbumTrack(HttpServletRequest request, AlbumTrack albumTrack) {
		String message;
		Song song = albumTrack.getSong();
		try {
			ArtistDAO artistDAO = new ArtistDAO(ds);
			artistDAO.saveSongArtists(song);
			SongDAO songDAO = new SongDAO(ds);
			if (!songDAO.exists(song)) {
				songDAO.save(song);
			}
			AlbumTrackDAO albumTrackDAO = new AlbumTrackDAO(ds);
			albumTrackDAO.save(albumTrack);
			message = "The album track " + song.getName() + " has been added successfully.";
		} catch (SQLException e) {
			message = "The album track " + song.getName() + " wasn't saved due to a database error.";
		} catch (IllegalArgumentException | ItemAlreadyExistsException e) {
			message = "The album track wasn't added. " + e.getMessage();
		}
		request.setAttribute("message", message);
		request.setAttribute("ordinal", albumTrack.getOrdinal());
		request.setAttribute("mainArtist", song.getInterpretingArtist().getName());
	}


}
