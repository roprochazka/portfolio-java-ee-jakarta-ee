package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import database.DBChange;
import database.DataType;
import database.HistoryDAO;

@WebServlet({ "/index", "/index.htm" })
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HistoryDAO historyDAO = new HistoryDAO(ds);
		String message = null;
		String menu = request.getParameter("menu");
		try {
			Map<DBChange, String> historyOfChangesMap = historyDAO.getHistoryOfChanges();
			Stream<Entry<DBChange, String>> changesStream = historyOfChangesMap.entrySet().stream();
			if (menu != null && !menu.isEmpty()) {
				changesStream = changesStream
						.filter(x -> x.getKey().getDataType() == DataType.valueOf(menu.toUpperCase()));
			}
			List<DBChange> orderedList = changesStream.map(x -> x.getKey())
					.sorted((x, y) -> y.getTimeStamp().compareTo(x.getTimeStamp())).collect(Collectors.toList());
			request.setAttribute("history", historyOfChangesMap);
			request.setAttribute("orderedHistory", orderedList);
		} catch (SQLException e) {
			message = e.getMessage();
			request.setAttribute("message", message);
		}
		request.setAttribute("menu", menu);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/index.jsp");
		dispatcher.forward(request, response);
	}
}
