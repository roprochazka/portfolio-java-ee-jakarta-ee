package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.InfoUtils;
import controller.utils.RequestHandler;
import database.AlbumDAO;
import database.SongDAO;
import model.Song;

@WebServlet("/findSong")
public class FindSongServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int idSong = RequestHandler.readId(request);
		String songName = request.getParameter("songName");
		if (idSong != 0 || (songName != null && !songName.isEmpty())) { // not allowing empty submit
			findSong(request, idSong, songName);
		}
		RequestHandler.forward("findSongPage.jsp?menu=Song", request, response);

	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void findSong(HttpServletRequest request, int idSong, String name) {
		String message = null;
		try {
			SongDAO songDAO = new SongDAO(ds);
			Map<Integer, Song> songs = new HashMap<>();
			if (idSong != 0) {
				Song song = songDAO.getSongById(idSong).orElseThrow(() -> {
					throw new IllegalArgumentException("The song doesn't exist.");
				});
				songs.put(idSong, song);
			} else {
				songs = songDAO.getSongsByName(name);
			}
			if (songs.isEmpty()) {
				message = "The song wasn't found.";
			}
			InfoUtils.addSongInfoToRequest(request, new AlbumDAO(ds), songs);
		} catch (SQLException | IllegalArgumentException e) {
			message = "The song wasn't found due to an error: " + e.getMessage();
		}
		if (message != null && !message.isEmpty()) {
			request.setAttribute("message", message); // set message if needed
		}
	}

}
