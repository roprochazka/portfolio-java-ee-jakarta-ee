package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.ArtistMemberDAO;
import model.Artist;
import model.Musician;

@WebServlet("/removeMusicianFromAlbum")
public class RemoveMusicianFromAlbumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Optional<Musician> optionalMusician = RequestHandler.readMusician(request);
		Optional<Artist> optionalArtist = RequestHandler.readArtist(request);
		int albumId = RequestHandler.readId(request);
		if (albumId != 0 && optionalMusician.isPresent() && optionalArtist.isPresent()
				&& UserUtils.userLoggedIn(request)) {
			Musician musician = optionalMusician.get();
			Artist artist = optionalArtist.get();
			removeMusicianFromAlbum(request, albumId, musician, artist);
		}
		RequestHandler.forward("findAlbum?id=" + albumId, request, response);
	}

	private void removeMusicianFromAlbum(HttpServletRequest request, int albumId, Musician musician, Artist artist) {
		String message = null;
		try {
			ArtistMemberDAO artistMemberDAO = new ArtistMemberDAO(ds);
			artistMemberDAO.removeArtistMember(albumId, musician, artist);
			message = "The musician " + musician.getName() + " " + musician.getSurname() + " has been removed.";
		} catch (SQLException e) {
			message = "The musician " + musician.getName() + " " + musician.getSurname()
					+ " wasn't removed due to an error in the database." + e.getMessage();
		} catch (IllegalArgumentException e) {
			message = e.getMessage();
		}
		if (message!=null && !message.isEmpty()) {
			request.setAttribute("message", message); // set message if needed
		}
	}
}
