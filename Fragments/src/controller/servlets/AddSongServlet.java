package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.ArtistDAO;
import database.SongDAO;
import exceptions.ItemAlreadyExistsException;
import model.Song;

@WebServlet("/addSong")
public class AddSongServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Optional<Song> optSong = RequestHandler.readSongExtended(request);
		if (optSong.isPresent() && UserUtils.userLoggedIn(request)) {
			// not allowing an empty submit
			Song song = optSong.get();
			addSong(request, song);
		}
		RequestHandler.forward("WEB-INF/addSongPage.jsp?menu=Song", request, response);
	}

	private void addSong(HttpServletRequest request, Song song) {
		String message = null;
		try {
			ArtistDAO artistDAO = new ArtistDAO(ds);
			artistDAO.saveSongArtists(song);
			SongDAO songDAO = new SongDAO(ds);
			int idSong = songDAO.save(song);
			message = "The song " + song.getName() + " has been saved successfully.";
			request.setAttribute("idSong", idSong); // forward the id of song to the jpg so that it can produce link
		} catch (SQLException e) {
			message = "The song " + song.getName() + " wasn't saved due to a database error." + e.getMessage();
		} catch (IllegalArgumentException e) {
			message = "The song " + song.getName() + " wasn't saved. " + e.getMessage();
		} catch (ItemAlreadyExistsException e) {
			message = "The song " + song.getName() + " wasn't saved because it's already in the database.";
		}
		request.setAttribute("message", message);
	}
}
