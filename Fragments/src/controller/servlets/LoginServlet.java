package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import database.UserDAO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Optional<String> optionalMessage = Optional.empty();
		String user = request.getParameter("user");
		String password = request.getParameter("password");
		if (user != null && !user.isEmpty() && password != null && !password.isEmpty()) { // not allowing an empty
																							// submit
			try {
				UserDAO userDAO = new UserDAO(ds);
				if (userDAO.checkCredentials(user, password)) {
					optionalMessage = Optional.of("User " + user + " signed in.");
					request.getSession().setAttribute("user", user);
				} else {
					optionalMessage = Optional.of("Could not sign in user " + user + ".");
				}
			} catch (SQLException e) {
				optionalMessage = Optional.of(e.getMessage());
			}
		}
		if (optionalMessage.isPresent()) {
			request.setAttribute("message", optionalMessage.get());
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("loginPage.jsp");
		dispatcher.forward(request, response);
	}
}
