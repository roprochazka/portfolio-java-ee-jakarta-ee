package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.AlbumDAO;
import model.Album;
import utils.ImageHandler;

@WebServlet("/updateAlbum")
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class UpdateAlbumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int idOldAlbum = RequestHandler.readId(request);
		Optional<Album> optAlbum = RequestHandler.readAlbum(request);
		boolean deleteCover = setDeleteCover(request);
		
		if (idOldAlbum > 0 && optAlbum.isPresent() && UserUtils.userLoggedIn(request)) {
			// not allowing an empty submit
			Album newAlbum = optAlbum.get();
			updateAlbum(request, idOldAlbum, deleteCover, newAlbum);
		}
		RequestHandler.forward("findAlbum?id=" + idOldAlbum, request, response);
	}

	private boolean setDeleteCover(HttpServletRequest request) {
		String deleteCoverParameter = request.getParameter("deleteCover");
		boolean deleteCover = false;
		if ("on".equals(deleteCoverParameter)) {
			deleteCover = true;
		}
		return deleteCover;
	}

	private void updateAlbum(HttpServletRequest request, int idOldAlbum, boolean deleteCover, Album newAlbum)
			throws IOException {
		String message = null;
		try {
			AlbumDAO albumDAO = new AlbumDAO(ds);
			albumDAO.update(idOldAlbum, newAlbum);
			if (newAlbum.getCover()!=null && newAlbum.getCover().length>0) {
				ImageHandler.saveCover(newAlbum, idOldAlbum);
			}
			if (deleteCover) {
				ImageHandler.deleteCover(idOldAlbum); // delete cover only if there's no exception
			}
			message = "The album " + newAlbum.getName() + " has been updated successfully.";
		} catch (SQLException e) {
			message = "The album " + newAlbum.getName() + " wasn't updated due to a database error.";
		}
		if (message!=null && !message.isEmpty()) {
			request.setAttribute("message", message);
		}
	}
}
