package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.AlbumDAO;
import model.Album;

@WebServlet("/addAlbum")
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class AddAlbumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Optional<Album> optAlbum = RequestHandler.readAlbum(request);
		if (optAlbum.isPresent() && UserUtils.userLoggedIn(request)) { // not allowing an empty submit
			Album album = optAlbum.get();
			addAlbum(request, album);
		}
		RequestHandler.forward("WEB-INF/addAlbumPage.jsp?menu=Album", request, response);
	}

	private void addAlbum(HttpServletRequest request, Album album) {
		String message = null;
		try {
			AlbumDAO albumDAO = new AlbumDAO(ds);
			int idAlbum = albumDAO.save(album);
			message = "The album " + album.getName() + " has been saved successfully.";
			request.setAttribute("idAlbum", idAlbum);
		} catch (SQLException | IOException e) {
			message = "The album " + album.getName() + " wasn't saved due to an error: " + e.getMessage();
		}
		request.setAttribute("message", message);;
	}
}
