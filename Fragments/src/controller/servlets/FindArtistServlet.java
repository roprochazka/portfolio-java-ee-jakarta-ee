package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.InfoUtils;
import controller.utils.RequestHandler;
import database.ArtistDAO;
import model.Artist;

@WebServlet("/findArtist")
public class FindArtistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int idArtist = RequestHandler.readId(request);
		Optional<Artist> optArtist = RequestHandler.readArtist(request);
		if (idArtist != 0 || optArtist.isPresent()) { // not allowing an empty submit
			findArtist(request, idArtist, optArtist);
		}
		RequestHandler.forward("findArtistPage.jsp?menu=Artist", request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private void findArtist(HttpServletRequest request, int id, Optional<Artist> optArtist) {
		String message = null;
		try {
			Artist foundArtist = null;			
			ArtistDAO artistDAO = new ArtistDAO(ds);
			if (id == 0) {
				Artist searchArtist = optArtist.get();
				foundArtist = artistDAO.getArtistByName(searchArtist.getName()).orElseThrow(() -> {
					throw new IllegalArgumentException("The artist wasn't found.");
				});
			} else {
				foundArtist = artistDAO.getArtistById(id).orElseThrow(() -> {
					throw new IllegalArgumentException("The id wasn't found.");
				});
			}
			InfoUtils.addArtistInfoToRequest(request, foundArtist, artistDAO);
		} catch (SQLException e) {
			message = "The artist " + request.getParameter("artistName")
					+ " wasn't found due to an error in the database: " + e.getMessage();
		} catch (IllegalArgumentException e) {
			message = e.getMessage();
		}
		if (message != null && !message.isEmpty()) {
			request.setAttribute("message", message); // set message if needed
		}
	}
}
