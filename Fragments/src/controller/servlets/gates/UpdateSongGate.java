package controller.servlets.gates;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.utils.UserUtils;

@WebServlet("/updateSongGate")
public class UpdateSongGate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String next = "loginPage.jsp";
		if (UserUtils.userLoggedIn(request)) {
			next = "WEB-INF/updateSongPage.jsp?menu=Song";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(next);
		dispatcher.forward(request, response);
	}
}
