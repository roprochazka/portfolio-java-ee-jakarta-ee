package controller.servlets.gates;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.UserUtils;

@WebServlet("/updateAlbumGate")
public class UpdateAlbumGate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String next = "loginPage.jsp";
		if (UserUtils.userLoggedIn(request)) {
			next = "WEB-INF/updateAlbumPage.jsp?menu=Album";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(next);
		dispatcher.forward(request, response);
	}
}
