package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.AlbumTrackDAO;
import model.Song;

@WebServlet("/removeSongFromAlbum")
public class RemoveAlbumTrackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int albumId = RequestHandler.readId(request);
		Optional<Song> optSong = RequestHandler.readSong(request);
		if (albumId != 0 && optSong.isPresent() && UserUtils.userLoggedIn(request)) {
			Song song = optSong.get();
			removeSongFromAlbum(request, albumId, song);
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("findAlbum?id=" + albumId);
		dispatcher.forward(request, response);
	}

	private void removeSongFromAlbum(HttpServletRequest request, int albumId, Song song) {
		String message = null;
		try {
			AlbumTrackDAO albumTrackDAO = new AlbumTrackDAO(ds);
			albumTrackDAO.removeAlbumTrack(albumId, song);
			message = "The track " + song.getName() + " has been removed.";
		} catch (SQLException | IllegalArgumentException e) {
			message = "The album track wasn't removed due to an error. " + e.getMessage();
		}
		if (message!=null && !message.isEmpty()) {
			request.setAttribute("message", message); 
		}
	}

}
