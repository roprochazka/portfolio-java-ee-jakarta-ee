package controller.servlets;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.DAO;
import database.DataType;

@WebServlet("/deleteMusician")
public class DeleteMusicianServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = RequestHandler.readId(request);
		if (id != 0 && UserUtils.userLoggedIn(request)) { // id must not be zero
			DAO dao = new DAO(ds);
			String message = dao.deleteItem(id, DataType.MUSICIAN);
			request.setAttribute("message", message);
		}

		RequestHandler.forward("findMusicianPage.jsp?menu=Artist", request, response);
	}
}
