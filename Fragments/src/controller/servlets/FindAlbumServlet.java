package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.InfoUtils;
import controller.utils.RequestHandler;
import database.AlbumDAO;
import model.Album;

@WebServlet("/findAlbum")
public class FindAlbumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String albumName = request.getParameter("title");
		int idAlbum = RequestHandler.readId(request);
		if (idAlbum != 0 || (albumName!=null && !albumName.isEmpty())) { // not allowing an empty submit
			findAlbum(request, albumName, idAlbum);
		}
		RequestHandler.forward("findAlbumPage.jsp?menu=Album", request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private void findAlbum(HttpServletRequest request, String albumName, int idAlbum) {
		String message = null;
		try {
			Map<Integer, Album> albums = new HashMap<>();
			AlbumDAO albumDAO = new AlbumDAO(ds);
			if (idAlbum == 0) {
				albums = albumDAO.getAlbumsByName(albumName);
			} else {
				Album album = albumDAO.getAlbumById(idAlbum).orElseThrow(() -> {
					throw new IllegalArgumentException("The album id doesn't exist.");
				});
				albums.put(idAlbum, album);
			}
			if (albums.isEmpty()) {
				message = "The album wasn't found.";
			}
			InfoUtils.addAlbumInfoToRequest(request, albums, albumDAO);
		} catch (SQLException e) {
			message = "The album wasn't found due to an error in the database: " + e.getMessage();
		} catch (IllegalArgumentException e) {
			message = e.getMessage();
		}
		if (message != null && !message.isEmpty()) {
			request.setAttribute("message", message); // set message if needed
		}
	}
}
