package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.InfoUtils;
import controller.utils.RequestHandler;
import database.AlbumDAO;
import database.MusicianDAO;
import model.Musician;

@WebServlet("/findMusician")
public class FindMusicianServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Optional<Musician> optMusician = RequestHandler.readMusician(request);
		int idMusician = RequestHandler.readId(request);
		if (idMusician != 0 || optMusician.isPresent()) {
			findMusician(request, optMusician, idMusician);
		}
		RequestHandler.forward("findMusicianPage.jsp?menu=Musician", request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private void findMusician(HttpServletRequest request, Optional<Musician> optMusician, int idMusician) {
		// not allowing empty submit
		String message = null;
		try {
			Musician foundMusician = null;
			MusicianDAO musicianDAO = new MusicianDAO(ds);
			if (idMusician == 0) {
				Musician searchMusician = optMusician.get();
				Optional<Musician> optionalFoundMusician = musicianDAO.getMusicianByNames(searchMusician.getName(),
						searchMusician.getSurname());
				foundMusician = optionalFoundMusician.orElseThrow(() -> {
					throw new IllegalArgumentException("The musician " + searchMusician.getName() + " "
							+ searchMusician.getSurname() + " wasn't found.");
				});
			} else {
				foundMusician = musicianDAO.getMusicianById(idMusician).orElseThrow(() -> {
					throw new IllegalArgumentException();
				});
			}
			InfoUtils.addMusicianInfoToRequest(request, foundMusician, musicianDAO, new AlbumDAO(ds));
		} catch (SQLException e) {
			message = "The musician " + request.getParameter("name") + " " + request.getParameter("surname")
					+ " wasn't found due to an error in the database." + e.getMessage();
		} catch (IllegalArgumentException e) {
			message = e.getMessage();
		}
		if (message!=null && !message.isEmpty()) {
			request.setAttribute("message", message); // set message if needed
		}
	}
}
