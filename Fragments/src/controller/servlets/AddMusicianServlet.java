package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.MusicianDAO;
import exceptions.ItemAlreadyExistsException;
import model.Musician;

@WebServlet("/addMusician")
public class AddMusicianServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Optional<Musician> optMusician = RequestHandler.readMusician(request);
		if (optMusician.isPresent() && UserUtils.userLoggedIn(request)) { // not allowing an empty submit
			Musician musician = optMusician.get();
			addMusician(request, musician);
		}
		RequestHandler.forward("WEB-INF/addMusicianPage.jsp?menu=Musician", request, response);
	}

	private void addMusician(HttpServletRequest request, Musician musician) {
		String message = null;
		try {
			MusicianDAO musicianDAO = new MusicianDAO(ds);
			musicianDAO.save(musician);
			message = "The musician " + musician.getName() + " " + musician.getSurname() + " has been saved successfully.";
			request.setAttribute("name", musician.getName());
			request.setAttribute("surname", musician.getSurname());
			// forwarding the name and surname of the musician to the jsp so that it can
			// produce a link
		} catch (SQLException e) {
			message = "The musician " + musician.getName() + " " + musician.getSurname() + " wasn't saved due to a database error.";
		} catch (ItemAlreadyExistsException e) {
			message = "The musician " + musician.getName() + " " + musician.getSurname() + " wasn't saved because he/she already exists.";
		}
		request.setAttribute("message", message);
	}
}
