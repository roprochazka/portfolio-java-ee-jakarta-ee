package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.ArtistDAO;
import database.SongDAO;
import exceptions.ItemAlreadyExistsException;
import model.Song;

@WebServlet("/updateSong")
public class UpdateSongServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int oldSongId = RequestHandler.readId(request);
		Optional<Song> optSong = RequestHandler.readSongExtended(request);
		if (oldSongId > 0 && optSong.isPresent() && UserUtils.userLoggedIn(request)) {
			// not allowing empty submit
			Song newSong = optSong.get();
			updateSong(request, oldSongId, newSong);
		}
		RequestHandler.forward("findSong?id=" + oldSongId, request, response);
	}

	private void updateSong(HttpServletRequest request, int oldSongId, Song newSong) {
		String message = null;
		try {
			ArtistDAO artistDAO = new ArtistDAO(ds);
			artistDAO.saveSongArtists(newSong);
			SongDAO songDAO = new SongDAO(ds);
			songDAO.update(oldSongId, newSong);
			message = "The song " + newSong.getName() + " has been updated successfully.";
		} catch (SQLException | ItemAlreadyExistsException e) {
			message = "The song has not been updated due to an error: " + e.getMessage();
		}
		if (message!=null && !message.isEmpty()) {
			request.setAttribute("message", message);
		}
	}
}
