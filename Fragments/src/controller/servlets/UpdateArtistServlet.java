package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.ArtistDAO;
import exceptions.ItemAlreadyExistsException;
import model.Artist;

@WebServlet("/updateArtist")
public class UpdateArtistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int idOldArtist = RequestHandler.readId(request);
		Optional<Artist> optNewArtist = RequestHandler.readArtist(request);
		if (idOldArtist > 0 && optNewArtist.isPresent() && UserUtils.userLoggedIn(request)) { // not allowing empty
																								// submit
			Artist newArtist = optNewArtist.get();
			updateArtist(request, idOldArtist, newArtist);
		}
		RequestHandler.forward("findArtist?id=" + idOldArtist, request, response);
	}

	private void updateArtist(HttpServletRequest request, int idOldArtist, Artist newArtist) {
		String message = null;
		try {
			ArtistDAO artistDAO = new ArtistDAO(ds);
			Artist oldArtist = artistDAO.getArtistById(idOldArtist).orElseThrow(() -> {
				throw new IllegalArgumentException("The artist doesn't exist.");
			});
			artistDAO.update(oldArtist, newArtist);
			message = "The artist " + newArtist.getName() + " was updated successfully.";
		} catch (SQLException e) {
			message = "The artist wasn't updated due to a database error: " + e.getMessage();
		} catch (ItemAlreadyExistsException | IllegalArgumentException e) {
			message = e.getMessage();
		}
		
		if (message != null && !message.isEmpty()) {
			request.setAttribute("message", message);
		}
	}
}
