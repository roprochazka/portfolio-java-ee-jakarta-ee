package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.MusicianDAO;
import exceptions.ItemAlreadyExistsException;
import model.Musician;

@WebServlet("/updateMusician")
public class UpdateMusicianServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int oldMusicianId = RequestHandler.readId(request);
		Optional<Musician> optMusician = RequestHandler.readMusician(request);
		if (oldMusicianId > 0 && optMusician.isPresent() && UserUtils.userLoggedIn(request)) {
			// not allowing empty submit
			Musician newMusician = optMusician.get();
			updateMusician(request, oldMusicianId, newMusician);
		}
		RequestHandler.forward("findMusician?id=" + oldMusicianId, request, response);
	}

	private void updateMusician(HttpServletRequest request, int oldMusicianId, Musician newMusician) {
		String message = null;
		try {
			MusicianDAO musicianDAO = new MusicianDAO(ds);
			musicianDAO.update(oldMusicianId, newMusician);
			message = "The song " + newMusician.getName() + " has been updated successfully.";
		} catch (SQLException | ItemAlreadyExistsException e) {
			message = "The song has not been updated due to an error: " + e.getMessage();
		}
		if (message!=null && !message.isEmpty()) {
			request.setAttribute("message", message);
		}
	}
}
