package controller.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import controller.utils.RequestHandler;
import controller.utils.UserUtils;
import database.ArtistDAO;
import exceptions.ItemAlreadyExistsException;
import model.Artist;

@WebServlet("/addArtist")
public class AddArtistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource
	DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Optional<Artist> optArtist = RequestHandler.readArtist(request);
		if (optArtist.isPresent() && UserUtils.userLoggedIn(request)) { // not allowing an empty click
			Artist artist = optArtist.get();
			addArtist(request, artist);
		}
		RequestHandler.forward("WEB-INF/addArtistPage.jsp?menu=Artist", request, response);
	}

	private void addArtist(HttpServletRequest request, Artist artist) {
		String message = null;
		try {
			ArtistDAO artistDAO = new ArtistDAO(ds);
			artistDAO.save(artist);
			message = "The artist " + artist.getName() + " was saved successfully.";
			request.setAttribute("name", artist.getName());
			// forwarding the name of the created artist so that jsp can create a link.
			// The artist is identified by his name.
		} catch (SQLException e) {
			message = "The artist " + artist.getName() + " wasn't saved due to a database error.";
		} catch (ItemAlreadyExistsException e) {
			message = "The artists " + artist.getName() + " wasn't saved because it already exists.";
		}
		request.setAttribute("message", message);
	}
}
