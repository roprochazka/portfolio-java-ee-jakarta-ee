package controller.utils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import database.AlbumDAO;
import database.ArtistDAO;
import database.MusicianDAO;
import model.Album;
import model.Artist;
import model.Musician;
import model.Song;

public class InfoUtils {

	public static void addAlbumInfoToRequest(HttpServletRequest request, Map<Integer, Album> albums, AlbumDAO albumDAO)
			throws SQLException {

		Map<Integer, List<Song>> albumSongsMap = new HashMap<>();
		Map<Integer, Set<Artist>> albumArtistsMap = new HashMap<>();
		Map<Integer, Map<Artist, Map<Musician, String>>> albumMusiciansMap = new HashMap<>();
		for (int albumId : albums.keySet()) {
			List<Song> albumSongs = albumDAO.getAlbumSongs(albumId);
			albumSongsMap.put(albumId, albumSongs);
			Set<Artist> albumArtists = albumDAO.getAlbumArtists(albumId);
			albumArtistsMap.put(albumId, albumArtists);
			Map<Artist, Map<Musician, String>> albumMusicians = albumDAO.getAlbumMusicians(albumId);
			albumMusiciansMap.put(albumId, albumMusicians);
		}
		request.setAttribute("albums", albums);
		request.setAttribute("albumSongs", albumSongsMap);
		request.setAttribute("albumArtists", albumArtistsMap);
		request.setAttribute("albumMusicians", albumMusiciansMap);
	}

	public static void addArtistInfoToRequest(HttpServletRequest request, Artist artist, ArtistDAO artistDAO)
			throws SQLException {
		Map<Integer, Album> artistAlbums = artistDAO.getArtistAlbums(artist);
		Map<Musician, Map<Integer, String>> artistMembers = artistDAO.getArtistMembers(artist);
		Map<Artist, Set<Musician>> relatedArtists = artistDAO.getRelatedArtists(artist);
		List<Integer> sortedIds = artistAlbums.entrySet().stream()
				.sorted((x, y) -> x.getValue().getYear() - y.getValue().getYear()).map(x -> x.getKey())
				.collect(Collectors.toList());
		Optional<Integer> optionalId = artistDAO.getId(artist);
		List<Song> ownInterpretedSongs = artistDAO.getArtistOwnInterpretedSongs(artist);
		List<Song> coveredSongs = artistDAO.getArtistCoveredSongs(artist);
		List<Song> songsCoveredByOthers = artistDAO.getArtistSongsCoveredByOthers(artist);
		int id = optionalId.get();
		request.setAttribute("id", id);
		request.setAttribute("artist", artist);
		request.setAttribute("sortedIds", sortedIds);
		request.setAttribute("artistAlbums", artistAlbums);
		request.setAttribute("artistMembers", artistMembers);
		request.setAttribute("relatedArtists", relatedArtists);
		request.setAttribute("ownInterpretedSongs", ownInterpretedSongs);
		request.setAttribute("coveredSongs", coveredSongs);
		request.setAttribute("songsCoveredByOthers", songsCoveredByOthers);
	}

	public static void addMusicianInfoToRequest(HttpServletRequest request, Musician musician, MusicianDAO musicianDAO, AlbumDAO albumDAO)
			throws SQLException {
		Map<Integer, Map<Artist, String>> musicianAlbumsMap = musicianDAO.getMusicianAlbums(musician);
		Map<Integer, Album> musicianAlbums = new HashMap<>();
		for (int albumId : musicianAlbumsMap.keySet()) {
			albumDAO.getAlbumById(albumId).ifPresent(album -> musicianAlbums.put(albumId, album));
		}
		List<Integer> sortedIds = musicianAlbums.keySet().stream()
				.sorted((x, y) -> musicianAlbums.get(x).getYear() - musicianAlbums.get(y).getYear())
				.collect(Collectors.toList());
		Optional<Integer> optionalId = musicianDAO.getId(musician);
		int id = optionalId.get();
		request.setAttribute("id", id);
		request.setAttribute("musician", musician);
		request.setAttribute("musicianAlbumsMap", musicianAlbumsMap);
		request.setAttribute("musicianAlbums", musicianAlbums);
		request.setAttribute("sortedIds", sortedIds);
	}

	public static void addSongInfoToRequest(HttpServletRequest request, AlbumDAO albumDAO, Map<Integer, Song> songs)
			throws SQLException {
		Map<Integer, Map<Integer, Album>> songAlbumsMap = new HashMap<>();
		for (int idSong : songs.keySet()) {
			Map<Integer, Album> songAlbums = albumDAO.getSongAlbums(songs.get(idSong));
			songAlbumsMap.put(idSong, songAlbums);
		}
		request.setAttribute("songs", songs);
		request.setAttribute("songAlbumsMap", songAlbumsMap);
	}
	

}
