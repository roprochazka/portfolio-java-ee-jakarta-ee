package controller.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import model.Album;
import model.AlbumTrack;
import model.Artist;
import model.ArtistMember;
import model.Musician;
import model.Nationality;
import model.Song;
import model.SongType;
import utils.DurationConverter;

public class RequestHandler {

	public static Optional<Album> readAlbum(HttpServletRequest request) throws IOException, ServletException {
		String title = request.getParameter("title");
		String review = request.getParameter("review");
		String yearParameter = request.getParameter("year");
		if (yearParameter == null || yearParameter.isEmpty()) {
			yearParameter = "0";
		}
		int year = Integer.valueOf(yearParameter);
		byte[] cover = readImageFromForm(request, "cover");
		Album album = new Album(title, year, review);
		if (cover != null && cover.length > 0) {
			album.setCover(cover);
		}
		if (album.getName() != null && !album.getName().isEmpty()) {
			return Optional.of(album);
		}
		return Optional.empty();
	}

	public static Optional<Song> readSong(HttpServletRequest request) {
		String songName = request.getParameter("songName");
		String originalArtistName = request.getParameter("originalArtist");
		String interpretingArtistName = request.getParameter("interpretingArtist");
		SongType type = SongType.valueOf(request.getParameter("type"));
		Artist originalArtist = new Artist(originalArtistName);
		Artist interpretingArtist = new Artist(interpretingArtistName);
		Song song = new Song(songName, originalArtist, interpretingArtist, type);
		if (song.getName() != null && !song.getName().isEmpty() && song.getOriginalArtist().getName() != null
				&& !song.getOriginalArtist().getName().isEmpty() && song.getInterpretingArtist().getName() != null
				&& !song.getInterpretingArtist().getName().isEmpty()) {
			return Optional.of(song);
		}
		return Optional.empty();
	}

	public static Optional<Song> readSongExtended(HttpServletRequest request) {
		Optional<Song> optSong = readSong(request);
		if (optSong.isPresent()) {
			Song song = optSong.get();
			String lyrics = request.getParameter("lyrics");
			String durationString = request.getParameter("duration");
			int duration = 0;
			if (durationString != null && !durationString.isEmpty()) {
				duration = DurationConverter.fromString(durationString);
			}
			song.setLyrics(lyrics);
			song.setDuration(duration);
			return Optional.of(song);
		}
		return Optional.empty();
	}

	public static Optional<Artist> readArtist(HttpServletRequest request) {
		String name = request.getParameter("artistName");
		String biography = request.getParameter("biography");
		Artist artist = new Artist(name, biography);
		if (name != null && !name.isEmpty()) {
			return Optional.of(artist);
		}
		return Optional.empty();
	}

	public static Optional<ArtistMember> readArtistMember(HttpServletRequest request) {
		int albumId = Integer.valueOf(request.getParameter("albumId"));
		Optional<Artist> optArtist = RequestHandler.readArtist(request);
		Optional<Musician> optMusician = RequestHandler.readMusician(request);
		String instrument = request.getParameter("instrument");
		if (optArtist.isPresent() && optMusician.isPresent() && instrument != null && !instrument.isEmpty()) {
			ArtistMember artistMember = new ArtistMember(albumId, optMusician.get(), optArtist.get(), instrument);
			return Optional.of(artistMember);
		}
		return Optional.empty();
	}

	public static Optional<Musician> readMusician(HttpServletRequest request) {
		String name = request.getParameter("musicianName");
		String surname = request.getParameter("musicianSurname");
		if (name != null && !name.isEmpty() && surname != null && !surname.isEmpty()) {
			Musician musician = new Musician(name, surname);
			String nationalityParameter = request.getParameter("nationality");
			if (nationalityParameter != null && !nationalityParameter.isEmpty()) {
				Nationality nationality = Nationality.valueOf(nationalityParameter);
				musician.setNationality(nationality);
			}
			return Optional.of(musician);
		}
		return Optional.empty();
	}

	public static Optional<AlbumTrack> readAlbumTrack(HttpServletRequest request) {
		Optional<Song> optSong = RequestHandler.readSong(request);
		if (optSong.isPresent()) {
			int albumId = Integer.valueOf(request.getParameter("albumId"));
			int ordinal = Integer.valueOf(request.getParameter("ordinal"));
			if (albumId != 0 && ordinal != 0) {
				return Optional.of(new AlbumTrack(optSong.get(), albumId, ordinal));
			}
		}
		return Optional.empty();
	}

	public static int readId(HttpServletRequest request) {
		String idParameter = request.getParameter("id");
		if (idParameter == null || idParameter.isEmpty()) {
			idParameter = "0";
		}
		int id = Integer.valueOf(idParameter);
		return id;
	}

	public static byte[] readImageFromForm(HttpServletRequest request, String inputName)
			throws IOException, ServletException {
		Part part = request.getPart(inputName);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			InputStream in = part.getInputStream();
			int i = 0;
			while ((i = in.read()) != -1) {
				baos.write(i);
			}
		} catch (IOException e) {
			throw new ServletException(e.getMessage());
		}
		byte[] image = baos.toByteArray();
		baos.flush();
		return image;
	}

	public static void forward(String where, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher(where);
		dispatcher.forward(request, response);
	}

}
