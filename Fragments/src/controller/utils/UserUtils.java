package controller.utils;

import javax.servlet.http.HttpServletRequest;

public class UserUtils {
	
	public static boolean userLoggedIn(HttpServletRequest request) {
		Object userObject = request.getSession().getAttribute("user");
		return userObject!=null;
	}

}
