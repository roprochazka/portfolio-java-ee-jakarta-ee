package utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import model.Album;

public class ImageHandler {

	public static final String IMAGE_EXTENSION = ".png";
	public static final String IMAGE_FOLDER = "../FragmentsImages/";

	public static void saveCover(Album album, int albumId) throws IOException {
		deleteCover(albumId);
		String fileName = albumId + IMAGE_EXTENSION;
		byte[] cover = album.getCover();
		if (cover != null && cover.length > 0) {
			saveImage(cover, IMAGE_FOLDER, fileName);
		}
	}

	public static void deleteCover(int albumId) {
		String fileName = albumId + IMAGE_EXTENSION;
		deleteImage(IMAGE_FOLDER, fileName);
	}

	private static void saveImage(byte[] image, String directory, String fileName) throws IOException {
		File directoryFile = new File(directory);
		if (!directoryFile.exists()) {
			directoryFile.mkdir();
		}
		// creating directory if it doesn't exist
		Path path = Paths.get(directory + fileName);
		Files.write(path, image);
	}

	private static void deleteImage(String directory, String fileName) {
		File imageFile = new File(directory + fileName);
		if (imageFile.exists()) {
			imageFile.delete();
		}
	}

}
