package utils;

public class DurationConverter {

	public static int fromString(String durationString) {
		String[] tokens = durationString.split(":");
		int hours = 0;
		int minutes = 0;
		int seconds = 0;
		if (tokens.length > 2) {
			hours = Integer.valueOf(tokens[0]);
			minutes = Integer.valueOf(tokens[1]);
			seconds = Integer.valueOf(tokens[2]);
		} else {
			minutes = Integer.valueOf(tokens[0]);
			seconds = Integer.valueOf(tokens[1]);
		}
		return hours * 3600 + minutes * 60 + seconds;

	}

	public static String toString(int duration) {
		int seconds = duration % 60;
		duration = duration / 60;
		int minutes = duration % 60;
		int hours = duration / 60;
		String secondsString = seconds < 10 ? "0" + seconds : "" + seconds;
		String minutesString = minutes < 10 && hours > 0 ? "0" + minutes : "" + minutes;
		if (hours > 0) {
			return hours + ":" + minutesString + ":" + secondsString;
		} else {
			return minutesString + ":" + secondsString;
		}
	}

	public static void main(String[] args) {
		System.out.println(fromString("05:23"));
		System.out.println(toString(323));
	}

}
