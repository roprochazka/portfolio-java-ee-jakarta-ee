package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import model.AlbumTrack;
import model.Song;

public class AlbumTrackDAO extends DAO {
	
	
	public AlbumTrackDAO(DataSource ds) {
		super(ds);
	}

	/**
	 * 
	 * @param albumId
	 * @return a List of album track ordinals which are already used by other songs
	 *         for that album
	 * @throws SQLException
	 */
	public List<Integer> getOrdinalsUsed(int albumId) throws SQLException {
		String sql = "SELECT ordinal FROM music.album_track WHERE album_id = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setInt(1, albumId);
			ResultSet rs = preparedStatement.executeQuery();
			List<Integer> resultList = new ArrayList<>();
			while (rs.next()) {
				int ordinal = rs.getInt("ordinal");
				resultList.add(ordinal);
			}
			return resultList;
		}
	}

	public void removeAlbumTrack(int albumId, Song song) throws SQLException, IllegalArgumentException {
		SongDAO songDAO = new SongDAO(ds);
		Optional<Integer> optSongId = songDAO.getId(song);
		int songId = optSongId.orElseThrow(() -> {
			throw new IllegalArgumentException("The song doesn't exist.");
		});
		String sql = "DELETE FROM music.album_track WHERE song_id = ? AND album_id = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, songId);
			ps.setInt(2, albumId);
			ps.execute();
		} catch (SQLException e) {
			throw new IllegalArgumentException("Error while deleting the album track! " + e.getMessage());
		}
	}

	/**
	 * Saves an album track in the database
	 * 
	 * @param albumTrack
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if the album track references a song which doesn't exist.
	 */
	public void save(AlbumTrack albumTrack) throws SQLException, IllegalArgumentException {
		SongDAO songDAO = new SongDAO(ds);
		Optional<Integer> optSongId = songDAO.getId(albumTrack.getSong());
		if (!optSongId.isPresent()) {
			throw new IllegalArgumentException("The song isn't in the database.");
		}
		int albumId = albumTrack.getAlbumId();
		int ordinal = albumTrack.getOrdinal();
		if (getOrdinalsUsed(albumId).contains(ordinal)) {
			throw new IllegalArgumentException("The track number for that album is already used by another song.");
		}
		String sql = "INSERT INTO music.album_track (song_id, album_id, ordinal) VALUES (?,?,?);";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, optSongId.get());
			ps.setInt(2, albumId);
			ps.setInt(3, ordinal);
			ps.execute();
		}
	}

}
