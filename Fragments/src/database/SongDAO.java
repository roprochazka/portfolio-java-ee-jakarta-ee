package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.sql.DataSource;

import exceptions.ItemAlreadyExistsException;
import model.Album;
import model.Artist;
import model.Song;
import model.SongType;

public class SongDAO extends DAO {

	public SongDAO(DataSource ds) {
		super(ds);
	}

	public Song getSongFromResultSet(ResultSet rs) throws SQLException {
		String name = rs.getString("name");
		ArtistDAO artistDAO = new ArtistDAO(ds);
		int originalArtistId = rs.getInt("original_artist_id");
		Artist originalArtist = artistDAO.getArtistById(originalArtistId).orElseThrow(() -> {
			throw new IllegalArgumentException("The original artist must exist.");
		});
		int interpretingArtistId = rs.getInt("interpreting_artist_id");
		Artist interpretingArtist = artistDAO.getArtistById(interpretingArtistId).orElseThrow(() -> {
			throw new IllegalArgumentException("The interpreting artist must exist.");
		});
		int duration = rs.getInt("duration");
		String lyrics = rs.getString("lyrics");
		String typeString = rs.getString("type");
		SongType type = SongType.valueOf(typeString);
		Song song = new Song(name, duration, originalArtist, interpretingArtist, lyrics, type);
		return song;
	}

	/**
	 * Finds all songs in the database that match the name in the argument.
	 * 
	 * @param searchName
	 * @return a map of the found songs. The keys are the ids of the albums. An
	 *         empty map if the album doesn't exist in the database.
	 * @throws SQLException
	 */

	public Map<Integer, Song> getSongsByName(String searchName) throws SQLException {
		String sql = "SELECT * FROM music.song WHERE lower(name) = ?;";
		Map<Integer, Song> songs = new HashMap<>();
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, searchName.toLowerCase());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Song song = getSongFromResultSet(rs);
				int id = rs.getInt("id_song");
				songs.put(id, song);
			}
		}
		return songs;
	}

	/**
	 * Find a song by its id.
	 * 
	 * @param idSong
	 * @return optional with the song if found, empty optional otherwise
	 * @throws SQLException
	 */

	public Optional<Song> getSongById(int idSong) throws SQLException {
		String sql = "SELECT * FROM music.song WHERE id_song = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, idSong);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				Song song = getSongFromResultSet(rs);
				return Optional.of(song);
			} else {
				return Optional.empty();
			}
		}
	}

	/**
	 * Saves a song to the database.
	 * 
	 * @param song
	 * @return id of the saved song.
	 * @throws SQLException
	 * @throws ItemAlreadyExistsException
	 *             if the song already exists in the database.
	 */

	public int save(Song song) throws SQLException, ItemAlreadyExistsException {
		if (exists(song)) {
			throw new ItemAlreadyExistsException("The song already exists.");
		}
		String sql = "INSERT INTO music.song(name, original_artist_id, interpreting_artist_id, lyrics, type, duration) VALUES(?,?,?,?,?,?);";
		Artist originalArtist = song.getOriginalArtist();
		Artist interpretingArtist = song.getInterpretingArtist();
		if (originalArtist == null || interpretingArtist == null) {
			throw new IllegalArgumentException("The song has to have an original and interpreting artist");
		}
		ArtistDAO artistDAO = new ArtistDAO(ds);
		Optional<Integer> optOriginalArtistId = artistDAO.getId(originalArtist);
		Optional<Integer> optInterpretingArtistId = artistDAO.getId(interpretingArtist);
		if (!optOriginalArtistId.isPresent() || !optInterpretingArtistId.isPresent()) {
			throw new IllegalArgumentException(
					"The original and interpreting artists have to be saved in the database.");
		}
		try (Connection con = ds.getConnection();
				PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, song.getName());
			ps.setInt(2, optOriginalArtistId.get());
			ps.setInt(3, optInterpretingArtistId.get());
			ps.setString(4, song.getLyrics());
			ps.setString(5, song.getType().toString());
			ps.setInt(6, song.getDuration());
			ps.execute();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				int id = generatedKeys.getInt(1);
				HistoryDAO historyDAO = new HistoryDAO(ds);
				historyDAO.saveToHistory(LocalDateTime.now(ZoneId.of("Europe/Prague")), ChangeType.INSERT, DataType.SONG, id);
				return id;
			} else {
				throw new SQLException("No key was generated.");
			}
		}
	}

	/**
	 * Updates a song.
	 * 
	 * @param idOldSong
	 *            the id of the song before the change
	 * @param newSong
	 *            the changed song
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if the artists of the song don't exist.
	 * @throws ItemAlreadyExistsException
	 */

	public void update(int idOldSong, Song newSong)
			throws SQLException, IllegalArgumentException, ItemAlreadyExistsException {
		String sql = "UPDATE music.song SET name=?, original_artist_id=?, interpreting_artist_id=?, type=?, duration= ?, lyrics = ? WHERE id_song = ?;";
		ArtistDAO artistDAO = new ArtistDAO(ds);
		Optional<Integer> optionalOriginalArtistId = artistDAO.getId(newSong.getOriginalArtist());
		Optional<Integer> optionalInterpretingArtistId = artistDAO.getId(newSong.getInterpretingArtist());
		if (!optionalOriginalArtistId.isPresent() || !optionalInterpretingArtistId.isPresent()) {
			throw new IllegalArgumentException("The original and interpreting artists must exist.");
		}
		if (exists(newSong)) {
			Optional<Song> optOldSong = getSongById(idOldSong);
			if (optOldSong.isPresent() && !optOldSong.get().equals(newSong)) {
				throw new ItemAlreadyExistsException(
						"Can't rename to an existing song with the same original and interpreting artists and the same type.");
			}
		}
		int originalArtistId = optionalOriginalArtistId.get();
		int interpretingArtistId = optionalInterpretingArtistId.get();
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setString(1, newSong.getName());
			preparedStatement.setInt(2, originalArtistId);
			preparedStatement.setInt(3, interpretingArtistId);
			preparedStatement.setString(4, newSong.getType().toString());
			preparedStatement.setInt(5, newSong.getDuration());
			preparedStatement.setString(6, newSong.getLyrics());
			preparedStatement.setInt(7, idOldSong);
			preparedStatement.execute();
			HistoryDAO historyDAO = new HistoryDAO(ds);
			historyDAO.saveToHistory(LocalDateTime.now(ZoneId.of("Europe/Prague")), ChangeType.UPDATE, DataType.SONG, idOldSong);
		}
	}

	/**
	 * Checks if a song exists in the database.
	 * 
	 * @param song
	 * @return true if exists, false otherwise.
	 * @throws SQLException
	 */

	public boolean exists(Song song) throws SQLException {
		return getId(song).isPresent();
	}

	public void deleteSong(int idSong) throws IllegalArgumentException, SQLException {
		Song song = getSongById(idSong).orElseThrow(() -> {
			throw new IllegalArgumentException("The album doesn't exist.");
		});
		AlbumDAO albumDAO = new AlbumDAO(ds);
		Map<Integer, Album> songAlbums = albumDAO.getSongAlbums(song);

		if (!songAlbums.isEmpty()) {
			throw new IllegalArgumentException("The song " + song.getName()
					+ " could not be deleted because there are albums containing this song in the database.");
		}
		String sql = "DELETE FROM music.song WHERE id_song = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, idSong);
			ps.execute();
			HistoryDAO historyDAO = new HistoryDAO(ds);
			historyDAO.deleteFromHistory(idSong, DataType.SONG);
		} catch (SQLException e) {
			throw new IllegalArgumentException("Could not delete the album " + song.getName() + " " + e.getMessage());
		}
	}

	/**
	 * Finds an id of a song in the database.
	 * 
	 * @param song
	 * @return optional with the id if found, empty optional otherwise.
	 * @throws SQLException
	 */

	public Optional<Integer> getId(Song song) throws SQLException {
		String sql = "SELECT id_song FROM music.song WHERE lower(name) = ? AND original_artist_id = ? AND interpreting_artist_id = ? AND type = ?;";
		Optional<Integer> optionalResult = Optional.empty();
		Artist originalArtist = song.getOriginalArtist();
		Artist interpretingArtist = song.getInterpretingArtist();
		if (originalArtist == null || interpretingArtist == null) {
			throw new IllegalArgumentException("The song has to have an original and an interpreting artist");
		}
		ArtistDAO artistDAO = new ArtistDAO(ds);
		Optional<Integer> optOriginalArtistId = artistDAO.getId(originalArtist);
		Optional<Integer> optInterpretingArtistId = artistDAO.getId(interpretingArtist);
		if (!optOriginalArtistId.isPresent() || !optInterpretingArtistId.isPresent()) {
			throw new IllegalArgumentException(
					"The original and interpreting artists have to be saved in the database.");
		}
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, song.getName().toLowerCase());
			ps.setInt(2, optOriginalArtistId.get());
			ps.setInt(3, optInterpretingArtistId.get());
			ps.setString(4, song.getType().toString());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				optionalResult = Optional.of(rs.getInt(1));
			}
			return optionalResult;
		}
	}

}
