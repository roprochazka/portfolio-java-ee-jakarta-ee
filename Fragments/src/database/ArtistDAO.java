package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import exceptions.ItemAlreadyExistsException;
import model.Album;
import model.Artist;
import model.Musician;
import model.Nationality;
import model.Song;

public class ArtistDAO extends DAO {

	public ArtistDAO(DataSource ds) {
		super(ds);
	}

	/**
	 * Deletes an artist from the database if there are no relations including the
	 * artist.
	 * 
	 * @param idArtist
	 * @throws IllegalArgumentException
	 *             if the artist doesn't exist or there are relations with the
	 *             artist
	 * @throws SQLException
	 */
	public void deleteArtist(int idArtist) throws IllegalArgumentException, SQLException {
		Artist artist = getArtistById(idArtist).orElseThrow(() -> {
			throw new IllegalArgumentException("The artist doesn't exist.");
		});
		List<Song> artistInterpretedSongs = getArtistInterpretedSongs(artist);
		List<Song> artistOriginalSongs = getSongsWrittenBy(artist);
		if (!artistInterpretedSongs.isEmpty()) {
			throw new IllegalArgumentException("The artist " + artist.getName()
					+ " could not be deleted because there are songs interpreted by the artist in the database.");
		}
		if (!artistOriginalSongs.isEmpty()) {
			throw new IllegalArgumentException("The artist " + artist.getName()
					+ " could not be deleted because there are songs written by the artist in the database.");
		}
		if (!getArtistMembers(artist).isEmpty()) {
			throw new IllegalArgumentException("The artist " + artist.getName()
					+ " could not be deleted because there are musicians accociated with this artist.");
		}
		String sql = "DELETE FROM music.artist WHERE id_artist = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, idArtist);
			ps.execute();
			HistoryDAO historyDAO = new HistoryDAO(ds);
			historyDAO.deleteFromHistory(idArtist, DataType.ARTIST);
		} catch (SQLException e) {
			throw new IllegalArgumentException(
					"Could not delete the artist. " + artist.getName() + " " + e.getMessage());
		}
	}

	/**
	 * Checks if an artists exists in the database.
	 * 
	 * @param artist
	 * @return true if exists, false otherwise.
	 * @throws SQLException
	 */

	public boolean exists(Artist artist) throws SQLException {
		return getId(artist).isPresent();
	}

	/**
	 * Finds all artists in the database.
	 * 
	 * @return a map of the artists (key is the artist id) ordered by names
	 * @throws SQLException
	 */
	public Map<Integer, Artist> getAllArtists() throws SQLException {
		String sql = "SELECT * FROM music.artist ORDER BY name;";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			ResultSet rs = preparedStatement.executeQuery();
			Map<Integer, Artist> artistMap = new HashMap<>();
			while (rs.next()) {
				int id = rs.getInt("id_artist");
				String name = rs.getString("name");
				String biography = rs.getString("biography");
				Artist artist = new Artist(name, biography);
				artistMap.put(id, artist);
			}
			return artistMap;
		}
	}

	/**
	 * Finds all albums of an artist (the artist is an interpreting artist on at
	 * least one song of the album).
	 * 
	 * @param artist
	 * @return a map, key is the album id and the value is the album
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if the artist doesn't exist
	 */
	public Map<Integer, Album> getArtistAlbums(Artist artist) throws SQLException, IllegalArgumentException {
		List<Song> artistOriginalSongs = getArtistInterpretedSongs(artist);
		Map<Integer, Album> artistAlbums = new HashMap<>();
		AlbumDAO albumDAO = new AlbumDAO(ds);
		for (Song song : artistOriginalSongs) {
			Map<Integer, Album> songAlbums = albumDAO.getSongAlbums(song);
			artistAlbums.putAll(songAlbums);
		}
		return artistAlbums;
	}

	/**
	 * Finds an artist by his id.
	 * 
	 * @param idArtist
	 * @return optional with the artist if found, empty optional otherwise
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public Optional<Artist> getArtistById(int idArtist) throws SQLException {
		String sql = "SELECT * FROM music.artist WHERE id_artist = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setInt(1, idArtist);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				String name = rs.getString("name");
				String biography = rs.getString("biography");
				Artist artist = new Artist(name, biography);
				return Optional.of(artist);
			} else {
				return Optional.empty();
			}
		}
	}

	/**
	 * Gets an artist from the database by it's name.
	 * 
	 * @param name
	 * @return optional with the artist if the artist was found. Empty optional
	 *         otherwise.
	 * @throws SQLException
	 */

	public Optional<Artist> getArtistByName(String name) throws SQLException {
		String sql = "SELECT * FROM music.artist WHERE lower(name) = ?;";
		Optional<Artist> optionalResult = Optional.empty();
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setString(1, name.toLowerCase());
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				String correctName = rs.getString("name");
				String biography = rs.getString("biography");
				Artist artist = new Artist(correctName, biography);
				optionalResult = Optional.of(artist);
			}
			return optionalResult;
		}
	}

	/**
	 * 
	 * @param artist
	 * @return the list of cover versions interpreted by the artist
	 * @throws IllegalArgumentException
	 * @throws SQLException
	 */
	public List<Song> getArtistCoveredSongs(Artist artist) throws IllegalArgumentException, SQLException {
		List<Song> coveredSongs = new ArrayList<>();
		List<Song> interpretedSongs = getArtistInterpretedSongs(artist);
		for (Song song : interpretedSongs) {
			if (!song.getOriginalArtist().equals(artist)) {
				coveredSongs.add(song);
			}
		}
		return coveredSongs;
	}

	/**
	 * Finds the songs which where interpreted by the artist from the argument.
	 * 
	 * @param artist
	 * @return a list of the songs which have the artist as an interpreting artist
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if the artist doesn't exist
	 */
	public List<Song> getArtistInterpretedSongs(Artist artist) throws SQLException, IllegalArgumentException {
		Optional<Integer> optionalArtistId = getId(artist);
		int artistId = optionalArtistId.orElseThrow(() -> {
			throw new IllegalArgumentException("The artist wasn't found!");
		});
		List<Song> songList = new ArrayList<>();

		String sql = "SELECT * FROM music.song WHERE interpreting_artist_id = ? ORDER BY name";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setInt(1, artistId);
			ResultSet rs = preparedStatement.executeQuery();
			SongDAO songDAO = new SongDAO(ds);
			while (rs.next()) {
				Song song = songDAO.getSongFromResultSet(rs);
				songList.add(song);
			}
		}
		return songList;
	}

	/**
	 * Finds all musicians who performed with an artist.
	 * 
	 * @param artist
	 * @return a map of maps. The key of the outer map is the musician, the value is
	 *         a map where the key is the id of the album and the value is a string
	 *         describing the instruments which the musician plays on the album with
	 *         that artist.
	 * @throws SQLException
	 */
	public Map<Musician, Map<Integer, String>> getArtistMembers(Artist artist) throws SQLException {
		Map<Musician, Map<Integer, String>> result = new HashMap<>();
		Optional<Integer> optionalArtistId = getId(artist);
		if (!optionalArtistId.isPresent()) {
			throw new IllegalArgumentException("The artist doesn't exist.");
		}
		int artistId = optionalArtistId.get();
		String sql = "SELECT * FROM music.artist_member WHERE artist_id = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setInt(1, artistId);
			ResultSet rs = preparedStatement.executeQuery();
			MusicianDAO musicianDAO = new MusicianDAO(ds);
			while (rs.next()) {
				int musicianId = rs.getInt("musician_id");
				String instrument = rs.getString("instrument");
				int albumId = rs.getInt("album_id");
				Optional<Musician> optMusician = musicianDAO.getMusicianById(musicianId);
				if (optMusician.isPresent()) {
					Musician musician = optMusician.get();
					if (!result.containsKey(musician)) {
						result.put(musician, new HashMap<>());
					}
					result.get(musician).put(albumId, instrument);
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * @param artist
	 * @return the list of the artist own interpreted songs, i.e. songs where both
	 *         interpretingArtist and originalArtist equal artist.
	 * @throws IllegalArgumentException
	 * @throws SQLException
	 */
	public List<Song> getArtistOwnInterpretedSongs(Artist artist) throws IllegalArgumentException, SQLException {
		List<Song> interpretedSongs = getArtistInterpretedSongs(artist);
		return interpretedSongs.stream().filter(x -> x.getOriginalArtist().equals(artist)).collect(Collectors.toList());
	}

	/**
	 * Finds all artists in the database filtered by nationality of their musician,
	 * i.e. an artist is included in the result if there's an musician who plays
	 * with the artist on at least one album with the nationality from the argument.
	 * 
	 * @param nationality
	 * @return a map of the artists (key is the artist id) ordered by names
	 * @throws SQLException
	 */

	public Map<Integer, Artist> getArtistsByNationality(Nationality nationality) throws SQLException {
		Map<Integer, Artist> resultMap = new HashMap<>();
		Map<Integer, Artist> allArtists = getAllArtists();
		for (int idArtist : allArtists.keySet()) {
			Set<Musician> artistMembers = new HashSet<>();
			artistMembers = getArtistMembers(allArtists.get(idArtist)).keySet();
			if (artistMembers.stream().anyMatch(musician -> musician.getNationality() == nationality)) {
				resultMap.put(idArtist, allArtists.get(idArtist));
			}
		}
		return resultMap;
	}

	/**
	 * 
	 * @param artist
	 * @return the list of songs which were interpreted by other artists
	 * @throws IllegalArgumentException
	 * @throws SQLException
	 */
	public List<Song> getArtistSongsCoveredByOthers(Artist artist) throws IllegalArgumentException, SQLException {
		List<Song> originalSongs = getSongsWrittenBy(artist);
		return originalSongs.stream().filter(x -> !x.getInterpretingArtist().equals(artist))
				.collect(Collectors.toList());
	}

	/**
	 * Finds the id of an artist in the database.
	 * 
	 * @param artist
	 * @return optional with the id if found, empty optional otherwise
	 * @throws SQLException
	 */

	public Optional<Integer> getId(Artist artist) throws SQLException {
		String sql = "SELECT id_artist FROM music.artist WHERE lower(name)=?;";
		Optional<Integer> optionalResult = Optional.empty();
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, artist.getName().toLowerCase());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				optionalResult = Optional.of(rs.getInt(1));
			}
			return optionalResult;
		}
	}

	/**
	 * Saves an artist to the database.
	 * 
	 * @param artist
	 * @return the id of the artist saved.
	 * @throws SQLException
	 * @throws ItemAlreadyExistsException
	 *             if the artist already exists in the database.
	 */

	public int save(Artist artist) throws SQLException, ItemAlreadyExistsException {
		if (exists(artist)) {
			throw new ItemAlreadyExistsException("The artist already exists.");
		}
		String sql = "INSERT INTO music.artist(name, biography) VALUES (?, ?)";
		try (Connection con = ds.getConnection();
				PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, artist.getName());
			ps.setString(2, artist.getBiography());
			ps.execute();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				int id = generatedKeys.getInt(1);
				HistoryDAO historyDAO = new HistoryDAO(ds);
				historyDAO.saveToHistory(LocalDateTime.now(ZoneId.of("Europe/Prague")), ChangeType.INSERT, DataType.ARTIST, id);
				return id;
			} else {
				throw new SQLException("No key was generated.");
			}
		}
	}

	/**
	 * Updates an artist.
	 * 
	 * @param oldArtist
	 *            the artist before the update
	 * @param newArtist
	 *            the changed artist
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if the old artist doesn't exist
	 * @throws ItemAlreadyExistsException
	 *             if the artist after the change already exists in the database
	 */
	public void update(Artist oldArtist, Artist newArtist)
			throws SQLException, IllegalArgumentException, ItemAlreadyExistsException {
		String sql = "UPDATE music.artist SET name=?, biography = ? WHERE id_artist = ?;";
		Optional<Integer> optionalArtistId = getId(oldArtist);
		if (!optionalArtistId.isPresent()) {
			throw new IllegalArgumentException();
		}
		if (exists(newArtist)) {
			if (!oldArtist.equals(newArtist)) {
				throw new ItemAlreadyExistsException("Can't change to an artist which already exists.");
			}
		}
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, newArtist.getName());
			ps.setString(2, newArtist.getBiography());
			ps.setInt(3, optionalArtistId.get());
			ps.execute();
			HistoryDAO historyDAO = new HistoryDAO(ds);
			historyDAO.saveToHistory(LocalDateTime.now(ZoneId.of("Europe/Prague")), ChangeType.UPDATE, DataType.ARTIST,
					optionalArtistId.get());
		}
	}

	/**
	 * Finds the songs which where written by the artist from the argument.
	 * 
	 * @param artist
	 * @return a set of the songs which have the artist as an original artist
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if the artist doesn't exist
	 */
	public List<Song> getSongsWrittenBy(Artist artist) throws SQLException, IllegalArgumentException {
		Optional<Integer> optionalArtistId = getId(artist);
		int artistId = optionalArtistId.orElseThrow(() -> {
			throw new IllegalArgumentException("The artist wasn't found!");
		});
		List<Song> songList = new ArrayList<>();
	
		String sql = "SELECT * FROM music.song WHERE original_artist_id = ? ORDER BY name";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setInt(1, artistId);
			ResultSet rs = preparedStatement.executeQuery();
			SongDAO songDAO = new SongDAO(ds);
			while (rs.next()) {
				Song song = songDAO.getSongFromResultSet(rs);
				songList.add(song);
			}
		}
		return songList;
	}

	public void saveSongArtists(Song song) throws SQLException, ItemAlreadyExistsException {
		// add original artist and interpreting artist to the database, if they
		// don't exist
		if (!exists(song.getOriginalArtist())) {
			save(song.getOriginalArtist());
		}
		if (!exists(song.getInterpretingArtist())) {
			save(song.getInterpretingArtist());
		}
	}

	/**
	 * Finds all artist which are related to the artist in the argument. Two artists
	 * are related if there's a musician who played with both of the artist on at
	 * least one album.
	 * 
	 * @param artist
	 * @return a map of sets. The key is the artist, the set includes all musicians
	 *         who are the intersection of the artist with the artist from the
	 *         argument.
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	public Map<Artist, Set<Musician>> getRelatedArtists(Artist artist) throws SQLException, IllegalArgumentException {
		ArtistDAO artistDAO = new ArtistDAO(ds);
		Set<Musician> artistMusicians = artistDAO.getArtistMembers(artist).keySet();
		Map<Integer, Map<Artist, String>> musicianAlbums = new HashMap<>();
		Map<Artist, Set<Musician>> result = new HashMap<>();
		MusicianDAO musicianDAO = new MusicianDAO(ds);
		for (Musician musician : artistMusicians) {
			musicianAlbums = musicianDAO.getMusicianAlbums(musician);
			for (int albumId : musicianAlbums.keySet()) {
				Set<Artist> artists = musicianAlbums.get(albumId).keySet();
				artists.remove(artist);
				for (Artist a : artists) {
					if (!result.containsKey(a)) {
						result.put(a, new HashSet<>());
					}
					result.get(a).add(musician);
	
				}
			}
		}
		return result;
	}

}
