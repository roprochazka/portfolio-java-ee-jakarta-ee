package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Set;

import javax.sql.DataSource;

import model.Artist;
import model.Musician;

public class ArtistMemberDAO extends DAO {
	
	public ArtistMemberDAO(DataSource ds) {
		super(ds);
	}

	/**
	 * Adds a member of an artist on an album with an instrument.
	 * 
	 * @param artist
	 * @param musician
	 * @param album
	 * @param instrument
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if the artist, musician or album doesn't exist or the album isn't
	 *             connected to the artist
	 */
	public void addArtistMember(Artist artist, Musician musician, int albumId, String instrument)
			throws SQLException, IllegalArgumentException {
		MusicianDAO musicianDAO = new MusicianDAO(ds);
		ArtistDAO artistDAO = new ArtistDAO(ds);
		Optional<Integer> optionalMusicianId = musicianDAO.getId(musician);
		Optional<Integer> optionalArtistId = artistDAO.getId(artist);
		if (!optionalMusicianId.isPresent() || !optionalArtistId.isPresent()) {
			throw new IllegalArgumentException("The album, musician and artist must exist.");
		}
		int musicianId = optionalMusicianId.get();
		int artistId = optionalArtistId.get();
		AlbumDAO albumDAO = new AlbumDAO(ds);
		Set<Artist> albumArtists = albumDAO.getAlbumArtists(albumId);
		if (!albumArtists.contains(artist)) {
			throw new IllegalArgumentException("The album doesn't belong to the artist.");
		}
	
		String sql = "INSERT INTO music.artist_member(album_id, musician_id, artist_id, instrument) VALUES (?,?,?,?);";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, albumId);
			ps.setInt(2, musicianId);
			ps.setInt(3, artistId);
			ps.setString(4, instrument);
			ps.execute();
		}
	}

	public void removeArtistMember(int idAlbum, Musician musician, Artist artist)
			throws SQLException, IllegalArgumentException {
		MusicianDAO musicianDAO = new MusicianDAO(ds);
		Optional<Integer> optIdMusician = musicianDAO.getId(musician);
		int idMusician = optIdMusician.orElseThrow(() -> {
			throw new IllegalArgumentException("The musician wasn't found.");
		});
		ArtistDAO artistDAO = new ArtistDAO(ds);
		Optional<Integer> optIdArtist = artistDAO.getId(artist);
		int idArtist = optIdArtist.orElseThrow(() -> {
			throw new IllegalArgumentException("The artist wasn't found.");
		});
		String sql = "DELETE FROM music.artist_member WHERE album_id = ? AND artist_id = ? AND musician_id = ?";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, idAlbum);
			ps.setInt(2, idArtist);
			ps.setInt(3, idMusician);
			ps.execute();
		} catch (SQLException e) {
			throw new IllegalArgumentException("Error while deleting the artist member! " + e.getMessage());
		}
	
	}

}
