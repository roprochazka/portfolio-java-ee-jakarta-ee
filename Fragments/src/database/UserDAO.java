package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

public class UserDAO extends DAO {
	
	public UserDAO(DataSource ds) {
		super(ds);
	}

	public boolean checkCredentials(String user, String password) throws SQLException {
		String sql = "SELECT * FROM music.user WHERE user = ? AND password = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, user);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
			return false;
		}
	}

}
