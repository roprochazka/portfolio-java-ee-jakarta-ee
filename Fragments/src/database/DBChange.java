package database;

import java.time.LocalDateTime;

public class DBChange {
	private LocalDateTime timeStamp;
	private DataType dataType;
	private ChangeType action;
	private int changedId;
	
	public DBChange(LocalDateTime timeStamp, DataType dataType, ChangeType action, int changedId) {
		this.timeStamp = timeStamp;
		this.dataType = dataType;
		this.action = action;
		this.changedId = changedId;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public DataType getDataType() {
		return dataType;
	}

	public ChangeType getAction() {
		return action;
	}

	public int getChangedId() {
		return changedId;
	}
}
