package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.sql.DataSource;

import model.Album;
import model.Artist;
import model.Musician;
import model.Song;

public class HistoryDAO extends DAO {
	
	public HistoryDAO(DataSource ds) {
		super(ds);
	}

	/**
	 * Saves a change to the history table.
	 * 
	 * @param instant
	 *            the timestamp of the change
	 * @param changeType
	 *            the type of the change
	 * @param dataType
	 *            the type of data the change refers to (album, song, musician, ...)
	 * @param id
	 *            the id of the respective data type
	 * @throws SQLException
	 */
	
	public void saveToHistory(LocalDateTime instant, ChangeType changeType, DataType dataType, int id)
			throws SQLException {
		String logSql = "INSERT INTO music.history(instant, change_type, data_type, changed_id) VALUES (?, ?, ?, ?);";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(logSql)) {
			Timestamp timeStamp = Timestamp.valueOf(instant);
			ps.setTimestamp(1, timeStamp);
			ps.setString(2, changeType.toString());
			ps.setString(3, dataType.toString());
			ps.setInt(4, id);
			ps.execute();
		} catch (SQLException e) {
			throw new SQLException("Could not save the change " + changeType + " to history." + e);
		}
	}

	public void deleteFromHistory(int id, DataType dataType) {
		String sql = "DELETE FROM music.history WHERE data_type=? AND changed_id = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, dataType.toString());
			ps.setInt(2, id);
			ps.execute();
		} catch (SQLException e) {
			throw new IllegalArgumentException("Error while deleting from history! " + e.getMessage());
		}
	}

	/**
	 * A history of changes.
	 * 
	 * @return a map, key is the respective change in the database (object
	 *         DBChange), value is a string describing the change
	 * @throws SQLException
	 */
	public Map<DBChange, String> getHistoryOfChanges() throws SQLException {
		Map<DBChange, String> resultList = new HashMap<>();
		String sql = "SELECT * FROM music.history ORDER BY instant desc;";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				Timestamp timeStamp = rs.getTimestamp("instant");
				LocalDateTime instant = timeStamp.toLocalDateTime();
				String changeTypeString = rs.getString("change_type");
				ChangeType changeType = ChangeType.valueOf(changeTypeString);
				String dataTypeString = rs.getString("data_type");
				DataType dataType = DataType.valueOf(dataTypeString);
				int changedId = rs.getInt("changed_id");
				DBChange change = new DBChange(instant, dataType, changeType, changedId);
				String descriptor = null;
	
				switch (dataType) {
				case ALBUM:
					AlbumDAO albumDAO = new AlbumDAO(ds);
					Optional<Album> optAlbum = albumDAO.getAlbumById(changedId);
					if (optAlbum.isPresent()) {
						Album album = optAlbum.get();
						descriptor = album.getName();
					}
					break;
				case ARTIST:
					ArtistDAO artistDAO = new ArtistDAO(ds);
					Optional<Artist> optArtist = artistDAO.getArtistById(changedId);
					if (optArtist.isPresent()) {
						Artist artist = optArtist.get();
						descriptor = artist.getName();
					}
	
					break;
				case MUSICIAN:
					MusicianDAO musicianDAO = new MusicianDAO(ds);
					Optional<Musician> optMusician = musicianDAO.getMusicianById(changedId);
					if (optMusician.isPresent()) {
						Musician musician = optMusician.get();
						descriptor = musician.getName() + " " + musician.getSurname();
					}
					break;
				case SONG:
					SongDAO songDAO = new SongDAO(ds);
					Optional<Song> optSong = songDAO.getSongById(changedId);
					if (optSong.isPresent()) {
						Song song = optSong.get();
						descriptor = song.getName() + " by " + song.getInterpretingArtist().getName();
					}
					break;
				}
				if (descriptor != null) {
					resultList.put(change, descriptor);
				}
			}
		} catch (SQLException e) {
			throw new SQLException("The history of changes could not be loaded due to an error in the database." + e);
		}
	
		return resultList;
	
	}

}
