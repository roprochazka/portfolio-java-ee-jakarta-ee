package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.sql.DataSource;

import exceptions.ItemAlreadyExistsException;
import model.Artist;
import model.Musician;
import model.Nationality;

public class MusicianDAO extends DAO {

	public MusicianDAO(DataSource ds) {
		super(ds);
	}

	/**
	 * Checks if a musician exists in the database.
	 * 
	 * @param musician
	 * @return true if exists, false otherwise.
	 * @throws SQLException
	 */
	
	public boolean exists(Musician musician) throws SQLException {
		return getId(musician).isPresent();
	}

	/**
	 * Updates a musician.
	 * 
	 * @param idOldMusician
	 * @param newMusician
	 * @throws SQLException
	 * @throws ItemAlreadyExistsException
	 *             if the new musician already exists
	 */
	
	public void update(int idOldMusician, Musician newMusician) throws SQLException, ItemAlreadyExistsException {
		String sql = "UPDATE music.musician SET name=?, surname= ?, nationality = ? WHERE id_musician = ?;";
		if (exists(newMusician)) {
			Optional<Musician> optOldMusician = getMusicianById(idOldMusician);
			if (optOldMusician.isPresent() && !optOldMusician.get().equals(newMusician)) {
				throw new ItemAlreadyExistsException("Can't update a musician to an existing one.");
			}
		}
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, newMusician.getName());
			ps.setString(2, newMusician.getSurname());
			ps.setString(3, newMusician.getNationality().toString());
			ps.setInt(4, idOldMusician);
			ps.execute();
			HistoryDAO historyDAO = new HistoryDAO(ds);
			historyDAO.saveToHistory(LocalDateTime.now(ZoneId.of("Europe/Prague")), ChangeType.UPDATE, DataType.MUSICIAN,
					idOldMusician);
		}
	}

	/**
	 * Saves a musician to the database.
	 * 
	 * @param musician
	 * @throws SQLException
	 * @throws ItemAlreadyExistsException
	 *             if the musician already exists in the database.
	 */
	
	public void save(Musician musician) throws SQLException, ItemAlreadyExistsException {
		if (exists(musician)) {
			throw new ItemAlreadyExistsException("The musician already exists.");
		}
		String sql = "INSERT INTO music.musician(name, surname, nationality) VALUES(?, ?, ?);";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, musician.getName());
			ps.setString(2, musician.getSurname());
			ps.setString(3, musician.getNationality().toString());
			ps.execute();
			int id = getId(musician).get();
			HistoryDAO historyDAO = new HistoryDAO(ds);
			historyDAO.saveToHistory(LocalDateTime.now(ZoneId.of("Europe/Prague")), ChangeType.INSERT, DataType.MUSICIAN, id);
		} catch (SQLException e) {
			throw new SQLException("Could not save musician due to an error in the database.");
		}
	}

	public void deleteMusician(int idMusician) throws IllegalArgumentException, SQLException {
		Musician musician = getMusicianById(idMusician).orElseThrow(() -> {
			throw new IllegalArgumentException("The album doesn't exist.");
		});
		Map<Integer, Map<Artist, String>> musicianAlbums = getMusicianAlbums(musician);
	
		if (!musicianAlbums.isEmpty()) {
			throw new IllegalArgumentException("The musician " + musician.getName() + " " + musician.getSurname()
					+ " could not be deleted because there are albums containing this song in the database.");
		}
		String sql = "DELETE FROM music.musician WHERE id_musician = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, idMusician);
			ps.execute();
			HistoryDAO historyDAO = new HistoryDAO(ds);
			historyDAO.deleteFromHistory(idMusician, DataType.MUSICIAN);
		} catch (SQLException e) {
			throw new IllegalArgumentException("Could not delete the musician " + musician.getName() + " "
					+ musician.getSurname() + " " + e.getMessage());
		}
	}

	/**
	 * Finds an id of the musician in the database.
	 * 
	 * @param musician
	 * @return optional with the musician if found, empty optional otherwise.
	 * @throws SQLException
	 */
	
	public Optional<Integer> getId(Musician musician) throws SQLException {
		String sql = "SELECT id_musician FROM music.musician WHERE name=? AND surname=?;";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setString(1, musician.getName());
			preparedStatement.setString(2, musician.getSurname());
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				return Optional.of(rs.getInt("id_musician"));
			} else {
				return Optional.empty();
			}
		}
	}

	/**
	 * Finds all albums connected to a musician.
	 * 
	 * @param musician
	 * @return a map of maps. The key of the outer map is the id of the album. The
	 *         value is a map where the key is the artist with whom the musician
	 *         plays on the album and value is a string describing the instruments
	 *         of the musician on the album with that artist
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if the musician doesn't exist
	 */
	public Map<Integer, Map<Artist, String>> getMusicianAlbums(Musician musician)
			throws SQLException, IllegalArgumentException {
		Map<Integer, Map<Artist, String>> result = new HashMap<>();
		Optional<Integer> optionalId = getId(musician);
		if (!optionalId.isPresent()) {
			throw new IllegalArgumentException("The musician doesn't exist.");
		}
		int musicianId = optionalId.get();
		String sql = "SELECT * FROM music.artist_member WHERE musician_id = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setInt(1, musicianId);
			ResultSet rs = preparedStatement.executeQuery();
			ArtistDAO artistDAO = new ArtistDAO(ds);
			while (rs.next()) {
				int albumId = rs.getInt("album_id");
				int artistId = rs.getInt("artist_id");
				String instrument = rs.getString("instrument");
				Optional<Artist> optArtist = artistDAO.getArtistById(artistId);
				if (optArtist.isPresent()) {
					Artist artist = optArtist.get();
					if (!result.keySet().contains(albumId)) {
						result.put(albumId, new HashMap<>());
					}
					result.get(albumId).put(artist, instrument);
				}
			}
		}
		return result;
	}

	/**
	 * Finds a musician by his id.
	 * 
	 * @param idMusician
	 * @return optional with the musician if found, empty optional otherwise
	 * @throws SQLException
	 */
	public Optional<Musician> getMusicianById(int idMusician) throws SQLException {
		String sql = "SELECT * FROM music.musician WHERE id_musician = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, idMusician);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String name = rs.getString("name");
				String surname = rs.getString("surname");
				Nationality nationality = Nationality.valueOf(rs.getString("nationality"));
				Musician musician = new Musician(name, surname, nationality);
				return Optional.of(musician);
			} else {
				return Optional.empty();
			}
		}
	}

	/**
	 * Gets a complete musician object from the database by a name and surname.
	 * 
	 * @param name
	 * @param surname
	 * @return optional of the musician if the musician was found. Empty optional
	 *         otherwise.
	 * @throws SQLException
	 */
	
	public Optional<Musician> getMusicianByNames(String name, String surname) throws SQLException {
		String sql = "SELECT * FROM music.musician WHERE lower(name)=? AND lower(surname)=?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, name.toLowerCase());
			ps.setString(2, surname.toLowerCase());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String correctName = rs.getString("name");
				String correctSurname = rs.getString("surname");
				Nationality nationality = Nationality.valueOf(rs.getString("nationality"));
				Musician musician = new Musician(correctName, correctSurname, nationality);
				return Optional.of(musician);
			} else {
				return Optional.empty();
			}
		}
	}

}
