package database;

import java.sql.SQLException;

import javax.sql.DataSource;

public class DAO {
	protected DataSource ds;
	
	public DAO(DataSource ds) {
		this.ds = ds;
	}

	public String deleteItem(int id, DataType dataType) {
		String message = null;
		try {
			switch (dataType) {
			case ALBUM:
				AlbumDAO albumDAO = new AlbumDAO(ds);
				albumDAO.deleteAlbum(id);
				break;
			case ARTIST:
				ArtistDAO artistDAO = new ArtistDAO(ds);
				artistDAO.deleteArtist(id);
				break;
			case MUSICIAN:
				MusicianDAO musicianDAO = new MusicianDAO(ds);
				musicianDAO.deleteMusician(id);
				break;
			case SONG:
				SongDAO songDAO = new SongDAO(ds);
				songDAO.deleteSong(id);
				break;
			default:
				break;
			}
			message = "The " + dataType.toString().toLowerCase() + " was deleted successfully.";
		} catch (SQLException | IllegalArgumentException e) {
			message = e.getMessage();
		}
		return message;
	}

}
