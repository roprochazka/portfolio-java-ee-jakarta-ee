package database;

public enum ChangeType {
	INSERT("added"),
	UPDATE("updated");
	
	private String description;
	
	private ChangeType(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}

}
