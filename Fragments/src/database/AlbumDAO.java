package database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.sql.DataSource;

import model.Album;
import model.Artist;
import model.Musician;
import model.Song;
import utils.ImageHandler;

public class AlbumDAO extends DAO {
	
	public AlbumDAO(DataSource ds) {
		super(ds);
	}

	/**
	 * Checks if an album exists in the database.
	 * 
	 * @param album
	 * @return true if exists, false otherwise.
	 * @throws SQLException
	 */
	
	public boolean exists(Album album) throws SQLException {
		return !getAlbumsByName(album.getName()).isEmpty();
	}

	/**
	 * Finds all artists of an album. An artist is connected with a song.
	 * 
	 * @param albumId
	 * @return
	 * @throws SQLException
	 */
	public Set<Artist> getAlbumArtists(int albumId) throws SQLException {
		List<Song> albumSongs = getAlbumSongs(albumId);
		Set<Artist> resultSet = new HashSet<>();
		for (Song albumSong : albumSongs) {
			resultSet.add(albumSong.getInterpretingArtist());
		}
		return resultSet;
	}

	/**
	 * Find an album by its id.
	 * 
	 * @param idAlbum
	 * @return optional with the album if found, empty optional otherwise
	 * @throws SQLException
	 */
	public Optional<Album> getAlbumById(int idAlbum) throws SQLException {
		String sql = "SELECT * FROM music.album WHERE id_album = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, idAlbum);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int year = rs.getInt("year");
				String title = rs.getString("name");
				String review = rs.getString("review");
				Album album = new Album(title, year, review);
				return Optional.of(album);
			} else {
				return Optional.empty();
			}
		}
	}

	/**
	 * Finds all musicians connected to an album.
	 * 
	 * @param albumId
	 * @return a map of maps. The key of the outer map is the artist with whom the
	 *         musician is performing on the album. The value is a map with keys
	 *         being the musicians and values being a string with the description of
	 *         instruments the artist is playing.
	 * @throws SQLException
	 */
	public Map<Artist, Map<Musician, String>> getAlbumMusicians(int albumId) throws SQLException {
		Map<Artist, Map<Musician, String>> result = new HashMap<>();
		String sql = "SELECT * FROM music.artist_member WHERE album_id = ?";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setInt(1, albumId);
			ResultSet rs = preparedStatement.executeQuery();
			ArtistDAO artistDAO = new ArtistDAO(ds);
			MusicianDAO musicianDAO = new MusicianDAO(ds);
			while (rs.next()) {
				int artistId = rs.getInt("artist_id");
				int musicianId = rs.getInt("musician_id");
				String instrument = rs.getString("instrument");
				Optional<Artist> optArtist = artistDAO.getArtistById(artistId);
				Optional<Musician> optMusician = musicianDAO.getMusicianById(musicianId);
				if (optArtist.isPresent() && optMusician.isPresent()) {
					Artist artist = optArtist.get();
					Musician musician = optMusician.get();
					if (!result.containsKey(artist)) {
						result.put(artist, new HashMap<>());
					}
					result.get(artist).put(musician, instrument);
				}
			}
		}
		return result;
	}

	/**
	 * Finds all albums in the database matching the name in the argument.
	 * 
	 * @param name
	 * @return a map of albums with that name. Keys are the ids of the albums from
	 *         the database. Empty map if no such album exists.
	 * @throws SQLException
	 */
	
	public Map<Integer, Album> getAlbumsByName(String name) throws SQLException {
		String sql = "SELECT * FROM music.album WHERE lower(name) = ?;";
		Map<Integer, Album> albums = new HashMap<>();
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, name.toLowerCase());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String correctName = rs.getString("name");
				int year = rs.getInt("year");
				String review = rs.getString("review");
				Album album = new Album(correctName, year, review);
				int id = rs.getInt("id_album");
				albums.put(id, album);
			}
		}
		return albums;
	}

	/**
	 * Finds songs of an album (connection by album tracks).
	 * 
	 * @param albumId
	 * @return a list of the songs ordered by ordinals of the albums tracks
	 * @throws SQLException
	 */
	public List<Song> getAlbumSongs(int albumId) throws SQLException {
		String sql = "SELECT * FROM music.album_track WHERE album_id = ? ORDER BY ordinal;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, albumId);
			List<Integer> songIds = new ArrayList<>();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				songIds.add(rs.getInt("song_id"));
			}
			List<Song> resultList = new ArrayList<>();
			SongDAO songDAO = new SongDAO(ds);
			for (Integer songId : songIds) {
				Optional<Song> optSong = songDAO.getSongById(songId);
				if (optSong.isPresent()) {
					resultList.add(optSong.get());
				}
			}
			return resultList;
		}
	}

	/**
	 * Finds all albums in the database.
	 * 
	 * @return a list of albums ordered by their year of issue.
	 * @throws SQLException
	 */
	public Map<Integer, Album> getAllAlbums() throws SQLException {
		String sql = "SELECT * FROM music.album ORDER BY year;";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			ResultSet rs = preparedStatement.executeQuery();
			Map<Integer, Album> resultList = new HashMap<>();
			while (rs.next()) {
				int id = rs.getInt("id_album");
				String name = rs.getString("name");
				int year = rs.getInt("year");
				String review = rs.getString("review");
				resultList.put(id, new Album(name, year, review));
			}
			return resultList;
		}
	}

	public void deleteAlbum(int idAlbum) throws IllegalArgumentException, SQLException {
		Album album = getAlbumById(idAlbum).orElseThrow(() -> {
			throw new IllegalArgumentException("The album doesn't exist.");
		});
		List<Song> albumSongs = getAlbumSongs(idAlbum);
		Set<Artist> albumArtists = getAlbumArtists(idAlbum);
		Map<Artist, Map<Musician, String>> albumMusicians = getAlbumMusicians(idAlbum);
	
		if (!albumSongs.isEmpty()) {
			throw new IllegalArgumentException("The album " + album.getName()
					+ " could not be deleted because there are songs connected to this album in the database.");
		}
		if (!albumArtists.isEmpty()) {
			throw new IllegalArgumentException("The album " + album.getName()
					+ " could not be deleted because there are artists associated with this album in the database.");
		}
		if (!albumMusicians.isEmpty()) {
			throw new IllegalArgumentException("The album " + album.getName()
					+ " could not be deleted because there are musicians accociated with this album in the database.");
		}
		String sql = "DELETE FROM music.album WHERE id_album = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, idAlbum);
			ps.execute();
			HistoryDAO historyDAO = new HistoryDAO(ds);
			historyDAO.deleteFromHistory(idAlbum, DataType.ALBUM);
			ImageHandler.deleteCover(idAlbum);
		} catch (SQLException e) {
			throw new IllegalArgumentException("Could not delete the album " + album.getName() + " " + e.getMessage());
		}
	}

	/**
	 * Saves an album to the database. There is no check for duplicity as there can
	 * be more albums of the same name and year.
	 * 
	 * @param album
	 * @return the id of the album
	 * @throws SQLException
	 * @throws IOException
	 *             when the image of the album could not be saved
	 */
	
	public int save(Album album) throws SQLException, IOException {
		String sql = "INSERT INTO music.album(name, year, review) VALUES (?, ?, ?)";
		try (Connection con = ds.getConnection();
				PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, album.getName());
			ps.setInt(2, album.getYear());
			ps.setString(3, album.getReview());
			ps.execute();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				int id = generatedKeys.getInt(1);
				HistoryDAO historyDAO = new HistoryDAO(ds);
				historyDAO.saveToHistory(LocalDateTime.now(ZoneId.of("Europe/Prague")), ChangeType.INSERT, DataType.ALBUM, id);
				ImageHandler.saveCover(album, id);
				return id;
			} else {
				throw new SQLException("No key was generated.");
			}
		}
	}

	/**
	 * Updates an album.
	 * 
	 * @param idOldAlbum
	 *            The id of the album before the change.
	 * @param newAlbum
	 *            The changed album.
	 * @throws SQLException
	 */
	
	public void update(int idOldAlbum, Album newAlbum) throws SQLException {
		String sql = "UPDATE music.album SET name=?, year= ?, review = ? WHERE id_album = ?;";
		try (Connection con = ds.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, newAlbum.getName());
			ps.setInt(2, newAlbum.getYear());
			ps.setString(3, newAlbum.getReview());
			ps.setInt(4, idOldAlbum);
			ps.execute();
			HistoryDAO historyDAO = new HistoryDAO(ds);
			historyDAO.saveToHistory(LocalDateTime.now(ZoneId.of("Europe/Prague")), ChangeType.UPDATE, DataType.ALBUM, idOldAlbum);
		}
	}

	/**
	 * 
	 * @param song
	 * @return a map of albums which include the album in their setlist, the key is
	 *         the album id.
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if the song doesn't exist.
	 */
	
	public Map<Integer, Album> getSongAlbums(Song song) throws SQLException, IllegalArgumentException {
		SongDAO songDAO = new SongDAO(ds);
		Optional<Integer> optionalSongId = songDAO.getId(song);
		if (!optionalSongId.isPresent()) {
			throw new IllegalArgumentException("The song doesn't exist.");
		}
		int songId = optionalSongId.get();
		Map<Integer, Album> albums = new HashMap<>();
		String sql = "SELECT * FROM  music.album_track WHERE song_id=?;";
		try (Connection con = ds.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(sql)) {
			preparedStatement.setInt(1, songId);
	
			ResultSet rs = preparedStatement.executeQuery();
			AlbumDAO albumDAO = new AlbumDAO(ds);
			while (rs.next()) {
				int albumId = rs.getInt("album_id");
				Optional<Album> optAlbum = albumDAO.getAlbumById(albumId);
				if (optAlbum.isPresent()) {
					Album album = optAlbum.get();
					albums.put(albumId, album);
				}
			}
			return albums;
		}
	}

}
