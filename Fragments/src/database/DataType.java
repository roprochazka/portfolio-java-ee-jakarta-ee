package database;

public enum DataType {
	ARTIST ("Artist"),
	SONG ("Song"),
	ALBUM ("Album"),
	MUSICIAN ("Musician");
	
	private String description;
	
	private DataType(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}

}
