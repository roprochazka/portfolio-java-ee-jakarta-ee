package exceptions;

public class ItemAlreadyExistsException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	private String message;

	public ItemAlreadyExistsException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
