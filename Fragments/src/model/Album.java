package model;

public class Album {
	
	private String name;
	private int year;
	private String review;
	private byte[] cover;

	public Album(String name, int year) {
		this.name = name;
		this.year = year;
	}
	
	public Album(String name, int year, String review) {
		this.name = name;
		this.year = year;
		this.review = review;
	}

	public String getName() {
		return name;
	}

	public int getYear() {
		return year;
	}

	public String getReview() {
		return review;
	}
	
	public byte[] getCover() {
		return cover;
	}

	public void setIssueDate(int year) {
		this.year = year;
	}

	public void setReview(String review) {
		this.review = review;
	}
	
	public void setCover(byte[] cover) {
		this.cover = cover;
	}
	
	public String[] getReviewParagraphs() {
		return review.split("\\$p\\$");
	}

	public boolean equals(Object other) { // the albums are the same iff they are the same object (There can be more albums of the same name.)
		return other == this;
	}

	@Override
	public String toString() {
		return "Album [name=" + name + ", year=" + year + "]";
	}
}
