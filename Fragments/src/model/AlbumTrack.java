package model;

public class AlbumTrack {
	private Song song;
	private int albumId;
	private int ordinal;
	
	public AlbumTrack(Song song, int albumId, int ordinal) {
		this.song = song;
		this.albumId = albumId;
		this.ordinal = ordinal;
	}

	public Song getSong() {
		return song;
	}

	public int getAlbumId() {
		return albumId;
	}

	public int getOrdinal() {
		return ordinal;
	}

	
	@Override
	public String toString() {
		return "AlbumTrack [song=" + song + ", album=" + albumId + ", ordinal=" + ordinal + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + albumId;
		result = prime * result + ordinal;
		result = prime * result + ((song == null) ? 0 : song.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AlbumTrack other = (AlbumTrack) obj;
		if (albumId != other.albumId) {
			return false;
		}
		if (ordinal != other.ordinal) {
			return false;
		}
		if (song == null) {
			if (other.song != null) {
				return false;
			}
		} else if (!song.equals(other.song)) {
			return false;
		}
		return true;
	}
	
	
	
	
}
