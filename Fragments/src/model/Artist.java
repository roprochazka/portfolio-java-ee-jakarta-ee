package model;

public class Artist {
	
	private String name;
	private String biography;
	
	public Artist(String name) {
		this.name = name;
	}
	
	public Artist(String name, String biography) {
		this(name);
		this.biography = biography;
	}

	public String getName() {
		return name;
	}
	
	public String getBiography() {
		return biography;
	}
	
	public String[] getBiographyParagraphs() {
		return biography.split("\\$p\\$");
	}
	
	public void setBiography(String biography) {
		this.biography = biography;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Artist other = (Artist) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Artist [name=" + name + "]";
	}
	
	

}
