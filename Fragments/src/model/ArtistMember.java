package model;

public class ArtistMember {
	private int albumId;
	private Musician musician;
	private Artist artist;
	private String instrument;

	public ArtistMember(int albumId, Musician musician, Artist artist, String instrument) {
		this.albumId = albumId;
		this.musician = musician;
		this.instrument = instrument;
		this.artist = artist;
	}
	
	public int getAlbumId() {
		return albumId;
	}

	public Musician getMusician() {
		return musician;
	}

	public String getInstrument() {
		return instrument;
	}
	
	public Artist getArtist() {
		return artist;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + albumId;
		result = prime * result + ((artist == null) ? 0 : artist.hashCode());
		result = prime * result + ((instrument == null) ? 0 : instrument.hashCode());
		result = prime * result + ((musician == null) ? 0 : musician.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ArtistMember other = (ArtistMember) obj;
		if (albumId != other.albumId) {
			return false;
		}
		if (artist == null) {
			if (other.artist != null) {
				return false;
			}
		} else if (!artist.equals(other.artist)) {
			return false;
		}
		if (instrument == null) {
			if (other.instrument != null) {
				return false;
			}
		} else if (!instrument.equals(other.instrument)) {
			return false;
		}
		if (musician == null) {
			if (other.musician != null) {
				return false;
			}
		} else if (!musician.equals(other.musician)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SongPerformance [albumId=" + albumId + ", musician=" + musician + ", instrument=" + instrument + "]";
	}
}
