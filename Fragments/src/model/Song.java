package model;

import utils.DurationConverter;

public class Song {
	private String name;
	private int duration;
	private Artist originalArtist;
	private Artist interpretingArtist;
	private String lyrics;
	private SongType type;

	public Song(String name, Artist originalArtist, Artist interpretingArtist, 
			SongType type) {
		this.name = name;
		this.originalArtist = originalArtist;
		this.interpretingArtist = interpretingArtist;
		this.type = type;
	}

	public Song(String name, int duration, Artist originalArtist, Artist interpretingArtist, String lyrics,
			SongType type) {
		this(name, originalArtist, interpretingArtist, type);
		this.duration = duration;
		this.lyrics = lyrics;
	}

	public String getName() {
		return name;
	}

	public int getDuration() {
		return duration;
	}

	public Artist getOriginalArtist() {
		return originalArtist;
	}

	public Artist getInterpretingArtist() {
		return interpretingArtist;
	}

	public String getLyrics() {
		return lyrics;
	}

	public SongType getType() {
		return type;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public void setOriginalArtist(Artist originalArtist) {
		this.originalArtist = originalArtist;
	}

	public void setInterpretingArtist(Artist interpretingArtist) {
		this.interpretingArtist = interpretingArtist;
	}

	public void setLyrics(String lyrics) {
		this.lyrics = lyrics;
	}

	public void setType(SongType type) {
		this.type = type;
	}
	
	public String[] getLyricsLines() {
		return lyrics.split("\\$r");
	}
	
	public String getDurationAsString() {
		return DurationConverter.toString(duration);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((interpretingArtist == null) ? 0 : interpretingArtist.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((originalArtist == null) ? 0 : originalArtist.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		// two songs are the same iff they have the same name and the same orig. artist
		// and the same interpreting artist and the same type
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Song other = (Song) obj;
		if (interpretingArtist == null) {
			if (other.interpretingArtist != null) {
				return false;
			}
		} else if (!interpretingArtist.equals(other.interpretingArtist)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (originalArtist == null) {
			if (other.originalArtist != null) {
				return false;
			}
		} else if (!originalArtist.equals(other.originalArtist)) {
			return false;
		}
		if (type != other.type) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Song [name=" + name + ", duration=" + duration + ", originalArtist=" + originalArtist
				+ ", interpretingArtist=" + interpretingArtist + ", type=" + type + "]";
	}

}
