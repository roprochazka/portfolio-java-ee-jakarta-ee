<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}</p>
	</c:otherwise>
</c:choose>

<div class="article">
	<c:if test="${not empty songs}">
		<c:forEach var="idSong" items="${songs.keySet()}">
			<h1 class="note">${songs.get(idSong).name}</h1>
			<c:if test="${not empty user}">
				<div class="standalone-link">
					<a
						href="updateSongGate?id=${idSong}&title=${songs.get(idSong).name}&originalArtist=${songs.get(idSong).originalArtist.name}&interpretingArtist=${songs.get(idSong).interpretingArtist.name}&type=${songs.get(idSong).type}&duration=${songs.get(idSong).getDurationAsString()}&lyrics=${songs.get(idSong).lyrics}">Update
						song</a>&nbsp;|&nbsp;<a href="deleteSong?id=${idSong}">Delete song</a>
				</div>
			</c:if>
			<ul>
				<li>Interpreted by: <a
					href="findArtist?artistName=${songs.get(idSong).interpretingArtist.name}">${songs.get(idSong).interpretingArtist.name}</a></li>
				<li>Original by: <a
					href="findArtist?artistName=${songs.get(idSong).originalArtist.name}">${songs.get(idSong).originalArtist.name}</a></li>
				<li>Recording type: ${songs.get(idSong).type}</li>
				<c:if test="${songs.get(idSong).duration!=0}">
					<li>Duration: ${songs.get(idSong).getDurationAsString()}</li>
				</c:if>
			</ul>

			<h2>Albums including this song</h2>
			<ul>
				<c:choose>
					<c:when test="${not empty songAlbumsMap.get(idSong)}">
						<c:forEach var="albumId"
							items="${songAlbumsMap.get(idSong).keySet()}">
							<li>${songAlbumsMap.get(idSong).get(albumId).year}&nbsp;-&nbsp;<a
								href="findAlbum?id=${albumId}">${songAlbumsMap.get(idSong).get(albumId).name}</a></li>
						</c:forEach>
					</c:when>
					<c:otherwise>
				This song doesn't appear on any album.
				</c:otherwise>
				</c:choose>
			</ul>
			<c:choose>
				<c:when test="${empty songs.get(idSong).lyrics}">
					<p>There are no lyrics for the song ${songs.get(idSong).name}.</p>
				</c:when>
				<c:otherwise>
					<p>
						<c:forEach var="v" items="${songs.get(idSong).getLyricsLines()}">
									${v}<br>
						</c:forEach>
					</p>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</c:if>
</div>
<div class="box">
	<form action="findSong" method="get">
		<h1>Find song</h1>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Song name:</td>
					<td><input type="text" name="songName" required="required"  pattern="[^&]*"></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="footer.jspf"%>

