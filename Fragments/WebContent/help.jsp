<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="header.jspf"%>

<div class="article">
	<h1 class="question">Help</h1>
	<p>The content of the site consists of: artists, albums, songs and
		musicians.</p>
	<p>An artist can be a band, project or individual artist.
		Musicians, on the contrary are human players/band members. Each song
		has an original artist, who wrote the song, and an interpreting
		artist, who is playing it. The type of the song is either STUDIO or
		LIVE. Each album may contain songs by various artists and of various
		types.</p>
	<p>A musician can be connected to an artist via an album on which
		he played. An artist is connected to an album via the songs performed
		by that artist on that album.</p>
	<p>An artist is related to another artist if there are albums on
		which a common musician played with either of the artists (the albums
		can be different). (For example: Bill Bruford played with Yes on
		"Close to the Edge" and with Genesis on "Seconds Live", therefore,
		Genesis and Yes are related via Bill Bruford.</p>
	<p>An instrument is not set for an artist but to the performance of
		the artist with an artist on an album. A musician can be credited
		differently for different artists on an album.</p>
	<p>An entry for an artist may contains the artist's biography. An
		entry for an album may contain the album's review. An entry for a song
		may contain the song's lyrics.</p>
	<p>An artist is defined uniquely by his name.</p>
	<p>There can be many albums with the same name, year and artists.</p>
	<p>A song is defined uniquely by its name, original artist,
		interpreting artist and type.</p>
	<p>A musician is defined uniquely by his name and surname.</p>
	<h2>Navigation</h2>
	<ul>
		<li>Use the find feature to search for artists, albums, songs and
			musicians. While searching for an album by its name please note that
			you can receive more results.</li>
		<li>Browse the site by following the links between various
			entries (for example you can jump to a musician page from an album
			page via the musicians links and from there to other artists with
			whom the musician played etc.).</li>
		<li>There is an alphabetical list of artists, which can be
			filtered by nationality. An artist is connected to a nationality if
			at least one musician of that nationality played with that artist.</li>
		<li>There is a list of albums categorized by years. Note that
			there may be more albums of the same name in a year, but each link
			leads to one of the individual albums.</li>

	</ul>
	<c:if test="${not empty user }">
		<h2>Managing data</h2>
		<p>Due to the various relations between the data entries, some
			ways of entering / removing data are more efficient or less time
			consuming that others. Pay attention especially to the information
			below:</p>
		<ul>
			<li>You can't add an artist to an album directly - it must be
				done by adding a song to the album which is interpreted by the
				artist.</li>
			<li>You CAN add a track to an album without creating the song
				and its artists first - they will be created authomatically.</li>
			<li>You can't add a musician to an artist directly - musicians
				have to be added to an album AND an artist on the albums page.</li>
			<li>You can't add a musician to an album without creating the
				musician first and adding a song to the album which is interpreted
				by the artist to whom the musician will belong on the album.</li>
			<li>You can't add an instrument to a musician directly - the
				information about the instrument must be provided when a musician is
				added to an artist and an album.</li>
			<li>You can't delete an artist if there are songs interpreted or
				written by the artist.</li>
			<li>You can't delete an album if there are songs or musicians
				added to it.</li>
			<li>You can't delete a musician if there are albums on which he
				plays.</li>
		</ul>
	</c:if>
	</div>

	<%@ include file="footer.jspf"%>