<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}</p>
	</c:otherwise>
</c:choose>
<c:if test="${not empty musician}">
	<div class="article">
		<h1 class="musician">${musician.name}&nbsp;${musician.surname}</h1>
		<p>Nationality: ${musician.nationality.country}</p>
		<c:if test="${not empty user}">
			<div class="standalone-link">
				<a
					href="updateMusicianGate?id=${id}&name=${musician.name}&surname=${musician.surname}&nationality=${musician.nationality}&country=${musician.nationality.country}">Update
					musician</a>&nbsp;|&nbsp;<a href="deleteMusician?id=${id}">Delete
					musician</a>
			</div>
		</c:if>
		<h2>Albums</h2>
		<ul>
			<c:choose>
				<c:when
					test="${not empty musicianAlbums && not empty musicianAlbumsMap && not empty sortedIds}">
					<c:forEach var="idAlbum" items="${sortedIds}">
						<li>${musicianAlbums.get(idAlbum).year}&nbsp;-&nbsp;<a
							href="findAlbum?id=${idAlbum}">${musicianAlbums.get(idAlbum).name}</a>
							<c:forEach var="artist"
								items="${musicianAlbumsMap.get(idAlbum).keySet()}">
								(with <a href="findArtist?artistName=${artist.name}">${artist.name}</a> on
										${musicianAlbumsMap.get(idAlbum).get(artist)})
							</c:forEach>
						</li>
					</c:forEach>
				</c:when>
				<c:otherwise>
				There are no albums associated with this musician in the database.
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</c:if>
<div class="box">
	<form action="findMusician" method="get">
		<h1>Find musician</h1>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Name:</td>
					<td><input type="text" name="musicianName" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td class="firstColumn">Surname:</td>
					<td><input type="text" name="musicianSurname" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="footer.jspf"%>

