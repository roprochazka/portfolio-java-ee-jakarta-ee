<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}</p>
	</c:otherwise>
</c:choose>
<c:if test="${not empty albums}">
	<c:forEach var="id" items="${albums.keySet()}">
	<img src="image?id=${id}" class="albumcover">
		<div class="article">
			<h1 class="cd">${albums.get(id).name}</h1>
			<!-- album artists -->
			<p>
				An album by:
				<c:choose>
					<c:when test="${not empty albumArtists.get(id)}">
						<c:forEach var="artist" items="${albumArtists.get(id)}">
							<a href="findArtist?artistName=${artist.name}">${artist.name}</a>
							<c:set var="albumArtist" value="${artist.name}" scope="session" />
						</c:forEach>
					</c:when>
					<c:otherwise>
						<i>unknown</i>
						<c:set var="number" value="1" scope="session" />
						<c:remove var="albumArtist" scope="session" />
					</c:otherwise>
				</c:choose>
				| Issued in ${albums.get(id).year}.
			</p>
			<c:if test="${not empty user}">
				<div class="standalone-link">
					<a
						href="updateAlbumGate?id=${id}&title=${albums.get(id).name}&year=${albums.get(id).year}&review=${albums.get(id).review}">Update
						album</a>&nbsp;|&nbsp;<a href="deleteAlbum?id=${id}">Delete album</a>
				</div>
			</c:if>
			<!-- album tracks -->
			<ol>
				<c:choose>
					<c:when test="${not empty albumSongs.get(id)}">

						<c:set var="number" value="${albumSongs.get(id).size()+1}"
							scope="session" />
						<c:forEach var="song" items="${albumSongs.get(id)}">
							<li><b><a
									href="findSong?songName=${song.name}&type=${song.type}
								&originalArtist=${song.originalArtist.name}&interpretingArtist=${song.interpretingArtist.name}">${song.name}</a>
							</b>&nbsp;${song.type}&nbsp;&nbsp;(<i>${song.originalArtist.name}</i>,&nbsp;interpreted
								by <i>${song.interpretingArtist.name})</i>&nbsp; <c:if
									test="${not empty user}">
									<small> <a
										href="removeSongFromAlbum?id=${id}&songName=${song.name}&type=${song.type}
								&originalArtist=${song.originalArtist.name}&interpretingArtist=${song.interpretingArtist.name}">Remove
											track from the album</a>
									</small>
								</c:if></li>
						</c:forEach>

					</c:when>
					<c:otherwise>
					There are no songs attached to this album.
					</c:otherwise>
				</c:choose>
			</ol>
			<c:if test="${not empty user}">
				<div class="standalone-link">
					<a
						href="addAlbumTrackGate?menu=Album&albumId=${id}&title=${albums.get(id).name}">Add
						a track to this album</a>
				</div>
			</c:if>
			<!-- musicians -->
			<ul>
				<c:choose>
					<c:when test="${not empty albumMusicians.get(id)}">
						<c:forEach var="artist" items="${albumMusicians.get(id).keySet()}">
							<b>${artist.name}</b>
							<c:forEach var="musician"
								items="${albumMusicians.get(id).get(artist).keySet()}">
								<li><a
									href="findMusician?musicianName=${musician.name}&musicianSurname=${musician.surname}">${musician.name}&nbsp;${musician.surname}</a>&nbsp;-
									${albumMusicians.get(id).get(artist).get(musician)} <c:if
										test="${not empty user}">
									&nbsp;&nbsp;<small><a
											href="removeMusicianFromAlbum?id=${id}&artistName=${artist.name}&musicianName=${musician.name}&musicianSurname=${musician.surname}">Remove
												musician from the album</a></small>
									</c:if></li>
							</c:forEach>
						</c:forEach>
					</c:when>
					<c:otherwise>
					There are no musicians associated with this album
					</c:otherwise>
				</c:choose>
			</ul>
			<c:if test="${not empty user}">
				<div class="standalone-link">
					<a
						href="addMusicianToAlbumGate?menu=Album&albumId=${id}&title=${albums.get(id).name}">Add
						a musician to the album</a>
				</div>
			</c:if>



			<c:choose>
				<c:when test="${empty albums.get(id).review}">
					<p>There is no review of the album
						&quot;${albums.get(id).name}&quot;.</p>
				</c:when>
				<c:otherwise>
					<c:forEach var="v" items="${albums.get(id).getReviewParagraphs()}">
						<p>${v}</p>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</div>
	</c:forEach>
</c:if>

<div class="box">
	<form action="findAlbum" method="get">
		<h1>Find album</h1>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Album title:</td>
					<td><input type="text" name="title" required="required"  pattern="[^&]*"></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="footer.jspf"%>

