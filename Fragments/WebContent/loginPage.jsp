<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}
		</p>
	</c:otherwise>
</c:choose>

<div class="box">
	<form action="login" method="post" accept-charset="UTF-8">
		<h1>Log in</h1>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">User name:</td>
					<td><input type="text" name="user" required="required"></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input type="password" name="password" required="required"></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="footer.jspf"%>

