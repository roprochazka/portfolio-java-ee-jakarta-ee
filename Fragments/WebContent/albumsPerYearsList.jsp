<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}</p>
	</c:otherwise>
</c:choose>

<div class="article">
	<h1 class="cd">Albums</h1>
	<div class="floating-box-container">
		<c:choose>
			<c:when
				test="${not empty yearsMap && not empty albums && not empty yearsList}">
				<c:forEach var="year" items="${yearsList}">
					<div class="floating-box">
						<h2>${year}</h2>
						<ul>
							<c:forEach var="albumId"
								items="${yearsMap.get(year)}">
								
								<li><a href="findAlbum?id=${albumId}">${albums.get(albumId).name}</a></li>
							</c:forEach>
						</ul>
					</div>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<p>No albums found.</p>
			</c:otherwise>
		</c:choose>
	</div>
</div>

<div class="box" style="clear: both">
	<form action="findAlbum" method="get">
		<h1>Find album</h1>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Album name:</td>
					<td><input type="text" name="title"></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="footer.jspf"%>

