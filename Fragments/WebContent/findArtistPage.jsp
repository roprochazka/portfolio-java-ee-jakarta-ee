<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}</p>
	</c:otherwise>
</c:choose>
<c:if test="${not empty artist}">

	<div class="article">
		<h1 class="band">${artist.name}</h1>
		<c:if test="${not empty user}">
			<div class="standalone-link">
				<a
					href="updateArtistGate?id=${id}&artistName=${artist.name}&biography=${artist.biography}">Update
					artist</a>&nbsp;|&nbsp;<a href="deleteArtist?id=${id}">Delete
					artist</a>
			</div>
		</c:if>
		<h2>Albums</h2>
		<ul>
			<c:choose>
				<c:when test="${not empty artistAlbums && not empty sortedIds}">
					<c:forEach var="idAlbum" items="${sortedIds}">
						<li>${artistAlbums.get(idAlbum).year}&nbsp;-&nbsp;<a
							href="findAlbum?id=${idAlbum}">${artistAlbums.get(idAlbum).name}</a></li>
					</c:forEach>
				</c:when>
				<c:otherwise>
				There are no albums of this artist in the database.
				</c:otherwise>
			</c:choose>
		</ul>
		<h2>Musicians</h2>
		<ul>
			<c:choose>
				<c:when test="${not empty artistMembers && not empty artistAlbums}">
					<c:forEach var="musician" items="${artistMembers.keySet()}">
						<li><a
							href="findMusician?musicianName=${musician.name}&musicianSurname=${musician.surname}">${musician.name}&nbsp;${musician.surname}</a>
							<ul>
								<c:forEach var="albumId"
									items="${artistMembers.get(musician).keySet()}">
									<li><b>${artistMembers.get(musician).get(albumId)}</b> on
										<a href="findAlbum?id=${albumId}">${artistAlbums.get(albumId).name}</a></li>
								</c:forEach>
							</ul></li>
					</c:forEach>
				</c:when>
				<c:otherwise>
				There are no musicians associated with this artist.
				</c:otherwise>
			</c:choose>
		</ul>
		<c:choose>
			<c:when test="${empty artist.biography}">
				<table>
					<tr>
						<td><p>The biography of the artist ${artist.name} is
								empty.</p></td>

					</tr>
				</table>
			</c:when>
			<c:otherwise>
				<table>
					<tr>
						<td><c:forEach var="v"
								items="${artist.getBiographyParagraphs()}">
								<p>${v}</p>
							</c:forEach></td>
					</tr>
				</table>
			</c:otherwise>
		</c:choose>
		<h2>Original songs interpreted by ${artist.name}</h2>
		<p class="roundbordered">
			<small> <c:choose>
					<c:when test="${not empty ownInterpretedSongs}">
						<c:forEach var="song" items="${ownInterpretedSongs}" varStatus="loop">
							<a
								href="findSong?songName=${song.name}&type=${song.type}&originalArtist=${song.originalArtist}&interpretingArtist=${song.interpretingArtist}">${song.name}</a> (${song.type})
								<c:if test="${!loop.last}">&middot;</c:if>
						</c:forEach>
						
					</c:when>
					<c:otherwise>There are no original songs of artist ${artist.name} interpreted by himself.</c:otherwise>
				</c:choose>
			</small>
		</p>
		<h2>Songs covered by ${artist.name}</h2>
		<p class="roundbordered">
			<small> <c:choose>
					<c:when test="${not empty coveredSongs}">
						<c:forEach var="song" items="${coveredSongs}" varStatus="loop">
							<a
								href="findSong?songName=${song.name}&type=${song.type}&originalArtist=${song.originalArtist}&interpretingArtist=${song.interpretingArtist}">${song.name}</a> (${song.type}) by
								 <a href="findArtist?artistName=${song.originalArtist.name}">${song.originalArtist.name}</a>
								<c:if test="${!loop.last}">&middot;</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>There are no songs covered by ${artist.name}.</c:otherwise>
				</c:choose>
			</small>
		</p>
		<h2>Songs of ${artist.name} covered by others</h2>
		<p class="roundbordered">
			<small> <c:choose>
					<c:when test="${not empty songsCoveredByOthers}">
						<c:forEach var="song" items="${songsCoveredByOthers}" varStatus="loop">
							<a
								href="findSong?songName=${song.name}&type=${song.type}&originalArtist=${song.originalArtist}&interpretingArtist=${song.interpretingArtist}">${song.name}</a> (${song.type}) by
								 <a href="findArtist?artistName=${song.interpretingArtist.name}">${song.interpretingArtist.name}</a>
								<c:if test="${!loop.last}">&middot;</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>There are no songs of ${artist.name} covered by others.</c:otherwise>
				</c:choose>
			</small>
		</p>
		
		
		<h2>Related artists</h2>
		<ul>
			<c:choose>
				<c:when test="${not empty relatedArtists}">
					<c:forEach var="artist" items="${relatedArtists.keySet()}">
						<li><a href="findArtist?artistName=${artist.name}">${artist.name}</a>
							(common musicians: <c:forEach var="commonMusician"
								items="${relatedArtists.get(artist)}">
								<a
									href="findMusician?musicianName=${commonMusician.name}&musicianSurname=${commonMusician.surname}">${commonMusician.name}&nbsp;${commonMusician.surname}</a>)
							</c:forEach></li>
					</c:forEach>
				</c:when>
				<c:otherwise>
				There are no artists related to this artist in the database.
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</c:if>
<div class="box">
	<form action="findArtist" method="get">
		<h1>Find artist</h1>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Artist name:</td>
					<td><input type="text" name="artistName" required="required"  pattern="[^&]*"></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="footer.jspf"%>

