<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}
			<c:if test="${not empty idSong}">
				<a href="findSong?id=${idSong}">See the page of the song</a>.
			</c:if>
		</p>
	</c:otherwise>
</c:choose>

<div class="box">
	<form action="addSong" method="post" >
		<h1>Add song</h1>
		<p>Obligatory fields are the song name, the original and
			interpreting artists and the type of the song. Optionally you can
			enter a duration and lyrics for the song. If any of the artists doesn't
			appear in the database, he will be created automatically. Make sure there are no typos.</p>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Song name:</td>
					<td><input type="text" name="songName" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Original artist:</td>
					<td><input type="text" name="originalArtist" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Interpreting artist:</td>
					<td><input type="text" name="interpretingArtist" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Type:</td>
					<td><select name="type" required="required">
							<option value="STUDIO">STUDIO</option>
							<option value="LIVE">LIVE</option>
					</select></td>
				</tr>
				<tr>
					<td>Duration:</td>
					<td><input type="text" name="duration"
						pattern="([0-9]:[0-5]|[0-5]){0,1}[0-9]:[0-5][0-9]"
						placeholder="Form: (HH:)MM:SS"></td>
				</tr>
				<tr>
					<td>Lyrics:</td>
					<td><textarea name="lyrics"
							placeholder="Enter the lyrics of the song. Use $r to finish line."></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="../footer.jspf"%>

