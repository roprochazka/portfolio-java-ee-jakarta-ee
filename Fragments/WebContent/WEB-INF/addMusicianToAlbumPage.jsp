<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}</p>
	</c:otherwise>
</c:choose>

<!-- this page has to receive the album id, the album title and the number of tracks to add as a parameter-->
<div class="box">
	<form
		action="addMusicianToAlbum?albumId=${param.albumId}&title=${param.title}"
		method="post">
		<h1>
			Add musician to the album &quot;<a
				href="findAlbum?id=${param.albumId}">${param.title}</a>&quot;
		</h1>
		<p>All fields are mandatory. The artist must exist and be
			associated with the album. The musician must already exist.</p>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Artist:</td>
					<td><input type="text" name="artistName" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Musician name:</td>
					<td><input type="text" name="musicianName" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Musician surname:</td>
					<td><input type="text" name="musicianSurname"
						required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Instrument:</td>
					<td><input type="text" name="instrument" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="../footer.jspf"%>

