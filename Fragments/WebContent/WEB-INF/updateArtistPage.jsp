<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../header.jspf"%>

<div class="box">
	<form action="updateArtist?id=${param.id}" method="post">
		<h1>Update artist</h1>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Artist name:</td>
					<td><input type="text" name="artistName" value="${param.artistName}" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Biography:</td>
					<td><textarea name="biography">${param.biography}</textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="../footer.jspf"%>

