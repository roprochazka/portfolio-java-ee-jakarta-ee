<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}
			<c:if test="${not empty idAlbum}">
				<a href="findAlbum?id=${idAlbum}">See the albums page</a>.
			</c:if>
		</p>
	</c:otherwise>
</c:choose>

<div class="box">
	<form action="addAlbum" method="post" accept-charset="UTF-8"
		enctype="multipart/form-data">
		<h1>Add album</h1>
		<p>Obligatory fields are the album title and the year of issue.
			Optionally you can enter a review for the album. The artists of the
			album will be added automatically based on the songs which you add to
			the album tracklist.</p>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Album title:</td>
					<td><input type="text" name="title" required="required" pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Year of issue:</td>
					<td><input type="number" min="1950" max="2099" step="1"
						value="2018" name="year" required="required"></td>
				</tr>
				<tr>
					<td>Album cover:</td>
					<td><input type="file" name="cover"></td>
				</tr>
				<tr>
					<td>Review:</td>
					<td><textarea name="review"
							placeholder="Enter the review of the album. Use $p$ to finish paragraphs."></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="../footer.jspf"%>

