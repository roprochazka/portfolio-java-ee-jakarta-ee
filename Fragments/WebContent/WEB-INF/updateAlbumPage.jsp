<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../header.jspf"%>

<div class="box">
	<form action="updateAlbum?id=${param.id}" method="post" enctype="multipart/form-data">
		<h1>Update album</h1>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Album title:</td>
					<td><input type="text" name="title" value="${param.title}" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Year of issue:</td>
					<td><input type="number" min="1950" max="2099" step="1"
						value="${param.year}" name="year" required="required"></td>
				</tr>
				<tr>
					<td>Change album cover:</td>
					<td><input type="file" name="cover"></td>
				</tr>
				<tr>
					<td>Delete album cover:</td>
					<td><input type="checkbox" name="deleteCover"></td>
				</tr>
				<tr>
					<td>Review:</td>
					<td><textarea name="review">${param.review}</textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="../footer.jspf"%>

