<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}
			<c:if test="${not empty name}">
				<a href="findArtist?artistName=${name}">See the artists page</a>.
			</c:if>
		</p>
	</c:otherwise>
</c:choose>

<div class="box">
	<form action="addArtist" method="post">
		<h1>Add artist</h1>
		<p>You have to enter the artists name, optionally add a biography
			of the artist.</p>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Artist name:</td>
					<td><input type="text" name="artistName" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Biography:</td>
					<td><textarea name="biography"
							placeholder="Enter the biography of the artist. Use $p$ to finish paragraphs."></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="../footer.jspf"%>

