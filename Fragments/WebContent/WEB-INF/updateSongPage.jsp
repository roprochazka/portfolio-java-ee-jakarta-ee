<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../header.jspf"%>


<div class="box">
	<form
		action="updateSong?id=${param.id}"
		method="post">
		<h1>Update song</h1>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Song name:</td>
					<td><input type="text" value="${param.title}" name="songName" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Original artist:</td>
					<td><input type="text" value="${param.originalArtist}" name="originalArtist" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Interpreting artist:</td>
					<td><input type="text" value="${param.interpretingArtist}" name="interpretingArtist" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Type:</td>
					<td><input type="text" value="${param.type}" name="type" required="required"></td>
				</tr>
				<tr>
					<td>Duration:</td>
					<td><input type="text" name="duration"
						pattern="([0-9]:[0-5]|[0-5]){0,1}[0-9]:[0-5][0-9]"
						value="${param.duration}"></td>
				</tr>
				<tr>
					<td>Lyrics:</td>
					<td><textarea name="lyrics">${param.lyrics}</textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="../footer.jspf"%>

