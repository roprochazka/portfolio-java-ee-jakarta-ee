<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../header.jspf"%>

<div class="article">
	<c:choose>
		<c:when test="${not empty message}">
			${message}
		</c:when>
		<c:otherwise>
			<c:if test="${not empty history && not empty orderedHistory}">
			<c:choose>
			<c:when test="${menu=='Artist'}">
			<h1 class="band">History of changes of artists</h1>
			</c:when>
			<c:when test="${menu=='Album'}">
			<h1 class="cd">History of changes of albums</h1>
			</c:when>
			<c:when test="${menu=='Song'}">
			<h1 class="note">History of changes of songs</h1>
			</c:when>
			<c:when test="${menu=='Musician'}">
			<h1 class="musician">History of changes of musicians</h1>
			</c:when>
			<c:otherwise>
			<h1 class="cd">History of changes</h1>
			</c:otherwise>
			</c:choose>
				
				<ul>
					<c:forEach var="change" items="${orderedHistory}">
						<li>${change.dataType.description}&nbsp;<a
							href="find${change.dataType.description}?id=${change.changedId}">${history.get(change)}</a>
							&nbsp;${change.action.description}
							on&nbsp;${change.timeStamp.toLocalDate()}&nbsp;at&nbsp;${change.timeStamp.getHour()}:<c:if
								test="${change.timeStamp.getMinute() < 10}">0</c:if>${change.timeStamp.getMinute()}.
						</li>
					</c:forEach>
				</ul>
			</c:if>
		</c:otherwise>
	</c:choose>
</div>

<%@ include file="../footer.jspf"%>

