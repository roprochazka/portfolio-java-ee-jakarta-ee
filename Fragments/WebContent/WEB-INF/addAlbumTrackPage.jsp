<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../header.jspf"%>

<c:choose>
	<c:when test="${empty message}">
	</c:when>
	<c:otherwise>
		<p>${message}</p>
		<c:set var="number" value="${ordinal+1}" scope="session" />
		<c:set var="albumArtist" value="${mainArtist}" scope="session" />
	</c:otherwise>
</c:choose>

<!-- this page has to receive the album id, the album title and the number of tracks to add as a parameter-->
<div class="box">
	<form action="addAlbumTrack?albumId=${param.albumId}&title=${param.title}" method="post">
		<h1>Add track to the album &quot;<a href="findAlbum?id=${param.albumId}">${param.title}</a>&quot;</h1>
		<p>All fields are mandatory. If the song and artist don't exist in the database, they will be created. Make sure there are no typos.</p>
		<table>
			<tbody>
				<tr>
					<td class="firstColumn">Track number:</td>
					<td><input type="number" min="0" max="1000" step="1"
						name="ordinal" value="${number}" required="required"></td>
				</tr>
				<tr>
					<td>Song title:</td>
					<td><input type="text" placeholder="song name" name="songName" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Interpreting artist:</td>
					<td><input type="text" placeholder="interpreted by" name="interpretingArtist" value="${albumArtist}" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Original artist:</td>
					<td><input type="text" placeholder="original by" name="originalArtist"  value="${albumArtist}" required="required"  pattern="[^&]*"></td>
				</tr>
				<tr>
					<td>Type:</td>
					<td><select name="type"><option value="STUDIO">STUDIO</option>
							<option value="LIVE">LIVE</option>
					</select></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit"><input type="reset"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<%@ include file="../footer.jspf"%>

